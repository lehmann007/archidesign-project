<?php
	include('../inc/verificationSession.php');
	require('../inc/connectpdo.php');
	require('../inc/fonction.php');
?>
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from wrappixel.com/demos/admin-templates/admin-pro/main/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Apr 2018 21:24:30 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    
    <title>Presentation | Admin Archi-Design</title>
	
	<?php include ('../inc/head.php') ?>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Admin Archi-Design</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <?php include ('nav.php') ?>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include ('aside.php') ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h1 class="text-themecolor">Accueil</h1>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Accueil</a></li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- Projects of the month -->
                <!-- ============================================================== -->
                <div class="row">
                    <div class="col-lg-12 col-md-12">
						<div class="card">
                            <div class="card-body">
								<?php $title = findTitle(); ?>
                                <h2 class="font-normal">TITRE ACCUEIL / Description</h2>
                                <h2><u>Titre</u></h2>
                                <h3 class="m-b-0 m-t-20"><?php echo $title->title; ?></h3>
								<br/>
								<h2><u>Description</u></h2>
                                <h4 class="m-b-0 m-t-20"><?php echo $title->description; ?></h4>
								<br/>
                                <div class="card-body text-center">
									<a href="grandTitreModification.php" class="m-t-10 m-b-20 waves-effect waves-dark btn btn-info btn-md btn-rounded">Modifier</a>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include ('footer.php') ?>
        </div>
    </div>
	<?php include ('../inc/javascript.php') ?>
</body>
</html>