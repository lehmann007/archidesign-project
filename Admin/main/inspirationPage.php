<?php
	include('../inc/verificationSession.php');
	require('../inc/connectpdo.php');
	require('../inc/fonction.php');
?>
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from wrappixel.com/demos/admin-templates/admin-pro/main/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Apr 2018 21:24:30 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    
    <title>Inspiration Page | Admin Archi-Design</title>
	
	<?php include ('../inc/head.php') ?>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Admin Archi-Design</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <?php include ('nav.php') ?>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include ('aside.php') ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h1 class="text-themecolor">Inspiration (Page)</h1>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Inspiration (Page)</a></li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Stats box -->
                <!-- ============================================================== -->
				
				
				
                <div class="row">
                    <div class="col-lg-6 col-md-12">
                        <div class="card">
						<?php $inspirationPage = findInspirationPage($_GET['id']); ?>
							<h2 class="card-title text-center">PRESENTATION PAGE</h2>
							<img class="card-img-top img-fluid auto-height" src="../../archidesign/assets/allImages/<?php echo $inspirationPage->image1; ?>" alt="<?php echo $inspirationPage->descriptionImage1; ?>">
                            <div class="card-body">
								<h2><u>Description de l'image</u></h2>
                                <h5 class="font-normal"><?php echo $inspirationPage->descriptionImage1; ?></h5>
							
								<h2><u>Titre</u></h2>
                                <h3 class="font-normal"><?php echo $inspirationPage->title; ?></h3>
								
								<h2><u>Introduction</u></h2>
                                <h5 class="font-normal"><?php echo $inspirationPage->paragraphe1; ?></h5>
								
								<h2><u>Paragraphe</u></h2>
                                <h5 class="font-normal"><?php echo $inspirationPage->paragraphe2; ?></h5>
								
								<div class="row">
									<div class="col-lg-6 col-md-6 m-b-20"><img src="../../archidesign/assets/allImages/<?php echo $inspirationPage->image2; ?>" class="img-responsive radius" alt="<?php echo $inspirationPage->descriptionImage2; ?>" /></div>
									<div class="col-lg-6 col-md-6 m-b-20"><img src="../../archidesign/assets/allImages/<?php echo $inspirationPage->image3; ?>" class="img-responsive radius" alt="<?php echo $inspirationPage->descriptionImage3; ?>" /></div>
								</div>
								
								<h2><u>Description de la premiere image</u></h2>
                                <h5 class="font-normal"><?php echo $inspirationPage->descriptionImage2; ?></h5>
								
								<h2><u>Description de la deuxieme image</u></h2>
                                <h5 class="font-normal"><?php echo $inspirationPage->descriptionImage3; ?></h5>
								
                                <div class="card-body text-center">
									<a href="inspirationPageModification.php?id=<?php echo $_GET['id'] ?>" class="m-t-10 m-b-20 waves-effect waves-dark btn btn-info btn-md btn-rounded">Modifier</a>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include ('footer.php') ?>
        </div>
    </div>
	<?php include ('../inc/javascript.php') ?>
</body>
</html>