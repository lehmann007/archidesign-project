<?php
	include('../inc/verificationSession.php');
	require('../inc/connectpdo.php');
	require('../inc/fonction.php');
?>
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from wrappixel.com/demos/admin-templates/admin-pro/main/pages-scroll.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Apr 2018 21:29:15 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    <title>Piece Page | Admin Archi-Design</title>
	<?php include ('../inc/head.php') ?>
</head>

<body class="fix-header card-no-border fix-sidebar">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Admin Archi-Design</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
		<header class="topbar">
            <?php include ('nav.php') ?>
        </header>
		<?php include ('aside.php') ?>
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
            <!-- ============================================================== -->
            <!-- Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            <div class="row page-titles">
                <div class="col-md-5 align-self-center">
                    <h1 class="text-themecolor">Piece (Page)</h1>
                </div>
                <div class="col-md-7 align-self-center">
                    <ol class="breadcrumb">
                        <li class="breadcrumb-item"><a href="javascript:void(0)">Piece (Page)</a></li>
                    </ol>
                </div>
            </div>
            <!-- ============================================================== -->
            <!-- End Bread crumb and right sidebar toggle -->
            <!-- ============================================================== -->
            
                <!-- ============================================================== -->
                <!-- Start Page Content -->
                <!-- ============================================================== -->
				<div class="row">
					<div class="col-lg-4 col-md-12">
                        <div class="card">
							<?php $piecePage = findPiecePage($_GET['id']); ?>
							<h2 class="card-title text-center">PRESENTATION PAGE</h2>
                            <div class="card-body">
								<h2><u>Sous - Titre</u></h2>
                                <h3 class="font-normal"><?php echo $piecePage->title; ?></h3>
								
								<h2><u>Presentation</u></h2>
                                <h5 class="font-normal"><?php echo $piecePage->presentation; ?></h5>
								
                                <div class="card-body text-center">
									<a href="piecePageModification.php?id=<?php echo $_GET['id'] ?>" class="m-t-10 m-b-20 waves-effect waves-dark btn btn-info btn-md btn-rounded">Modifier</a>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
				
                <div class="row">
                    <div class="col-12">
						<h2 class="card-title text-center">INFORMATION PIECE (Information - Paragraphe)</h2>
						<?php $pieceInformations = findPieceInformation($_GET['id']); $j = 1;?>
						<?php foreach($pieceInformations AS $pieceInformation){ ?>
                        <div class="card">
                            <div class="card-body">
                                <h4 class="card-title"><?php echo $pieceInformation->information; ?></h4>
                                <div class="row">
                                    <div class="col-lg-4 col-md-3"><img src="../../archidesign/assets/allImages/<?php echo $pieceInformation->image; ?>" class="img-fluid" alt="<?php echo $pieceInformation->descriptionImage; ?>" /></div>
                                    <div class="col-lg-8 col-md-9" id="slimtest<?php echo $j; ?>" style="height: 250px;">
                                        <div>
											<h5><?php echo $pieceInformation->paragraphe; ?></h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
							<div class="card-body text-center">
								<a href="piecePageInformationModification.php?id=<?php echo $pieceInformation->id; ?>" class="m-t-10 m-b-20 waves-effect waves-dark btn btn-info btn-md btn-rounded">Modifier</a>
							</div>
                        </div>
						<?php $j++; } ?>
                    </div>
                </div>
            </div>
            <?php include ('footer.php') ?>
        </div>
        <!-- ============================================================== -->
        <!-- End Page wrapper  -->
        <!-- ============================================================== -->
    </div>
    <?php include ('../inc/javascript.php') ?>
	<script type="text/javascript">
		<?php
		$k = 1;
		foreach($pieceInformations AS $pieceInformation){ ?>
			$('#slimtest<?php echo $k?>').perfectScrollbar();
		<?php $k++; } ?>
    </script>
</body>


<!-- Mirrored from wrappixel.com/demos/admin-templates/admin-pro/main/pages-scroll.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Apr 2018 21:29:15 GMT -->
</html>