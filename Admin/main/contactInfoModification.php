<?php
	include('../inc/verificationSession.php');
	require('../inc/connectpdo.php');
	require('../inc/fonction.php');
?>
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from wrappixel.com/demos/admin-templates/admin-pro/main/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Apr 2018 21:24:30 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    
    <title>Contact Info Modification | Admin Archi-Design</title>
	<?php include ('../inc/head.php') ?>
</head>

<body class="fix-header card-no-border fix-sidebar">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Admin Archi-Design</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <?php include ('nav.php') ?>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include ('aside.php') ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h1 class="text-themecolor">Contact</h1>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Stats box -->
                <!-- ============================================================== -->
				
				
				
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-body">
                                <h2 class="card-title">Menu</h2>
								<?php $contactInfo = findContactInfoById($_GET['id']); ?>
                                <form class="form-material m-t-40" action="../inc/contactInfoModification.php" method="POST">
									<div class="form-group" hidden>
                                        <input name="id" type="text" class="form-control form-control-line" value="<?php echo $_GET['id']; ?>">
									</div>
                                    <div class="form-group">
                                        <label>Nom</label>
                                        <input name="nom" type="text" class="form-control form-control-line" value="<?php echo $contactInfo->nom; ?>">
									</div>
									<div class="form-group">
                                        <label>Rue</label>
                                        <input name="rue" type="text" class="form-control form-control-line" value="<?php echo $contactInfo->rue; ?>">
									</div>
									<div class="form-group">
                                        <label>Lieu</label>
                                        <input name="lieu" type="text" class="form-control form-control-line" value="<?php echo $contactInfo->lieu; ?>">
									</div>
									<div class="form-group">
                                        <label>Contact</label>
                                        <input name="phone" type="text" class="form-control form-control-line" value="<?php echo $contactInfo->phone; ?>">
									</div>
									<div class="form-group">
                                        <label>Email</label>
                                        <input name="email" type="text" class="form-control form-control-line" value="<?php echo $contactInfo->email; ?>">
									</div>
									<div class="form-group m-b-0">
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Modifier</button>
                                    </div>
                                </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include ('footer.php') ?>
        </div>
    </div>
	<?php include ('../inc/javascript.php') ?>
</body>
</html>