<?php
	include('../inc/verificationSession.php');
	require('../inc/connectpdo.php');
	require('../inc/fonction.php');
?>
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from wrappixel.com/demos/admin-templates/admin-pro/main/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Apr 2018 21:24:30 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    
    <title>Eclairage modification | Admin Archi-Design</title>
	<?php include ('../inc/head.php') ?>
	<link rel="stylesheet" href="../assets/plugins/dropify/dist/css/dropify.min.css">
	 <link href="../assets/plugins/summernote/dist/summernote.css" rel="stylesheet" />
</head>

<body class="fix-header card-no-border fix-sidebar">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Admin Archi-Design</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <?php include ('nav.php') ?>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include ('aside.php') ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h1 class="text-themecolor">Eclairage</h1>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Stats box -->
                <!-- ============================================================== -->
				
				<div class="row">
                    <div class="col-lg-12 col-md-12">
						<?php 
							$eclairage = findEclairage();
						?>
                        <form class="form-material m-t-40" action="../inc/eclairageModification.php" method="POST" enctype="multipart/form-data">
							<h4 class="card-title">Image</h4>
							<input name="image" type="file" id="input-file-now-custom-1" class="dropify"/>
						
							<div class="form-group">
								<h4>Description image</h4>
								<input name="descriptionImage" type="text" class="form-control form-control-line" value="<?php echo $eclairage->descriptionImage ?>">
							</div>
							
							<div class="form-group">
								<h4>Titre</h4>
								<input name="titre" type="text" class="form-control form-control-line" value="<?php echo $eclairage->title ?>">
							</div>
							
							<div class="form-group">
								<h4>Description</h4>
								<div class="col-12">
									<div class="card">
										<div class="card-body">
											<div class="summernote">
												<h3></h3>
												<textarea name="description" class="textarea_editor form-control" rows="15" placeholder="<?php echo $eclairage->description ?>"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="form-group">
								<h4>Paragraphe</h4>
								<div class="col-12">
									<div class="card">
										<div class="card-body">
											<div class="summernote">
												<h3></h3>
												<textarea name="paragraphe" class="textarea_editor form-control" rows="15" placeholder="<?php echo $eclairage->paragraphe ?>"></textarea>
											</div>
										</div>
									</div>
								</div>
							</div>
							
							<div class="form-group m-b-0">
							<div class="text-center">
								<button type="submit" class="btn btn-info waves-effect waves-light m-t-10">Modifier</button>
							</div>
							</div>
						</form>
                    </div>
                </div>
            </div>
            <?php include ('footer.php') ?>
        </div>
    </div>
	<?php include ('../inc/javascript.php') ?>
	<script src="../assets/plugins/dropify/dist/js/dropify.min.js"></script>
	<script src="../assets/plugins/summernote/dist/summernote.min.js"></script>
	<script>
    jQuery(document).ready(function() {

        $('.summernote').summernote({
            height: 350, // set editor height
            minHeight: null, // set minimum height of editor
            maxHeight: null, // set maximum height of editor
            focus: false // set focus to editable area after initializing summernote
        });

        $('.inline-editor').summernote({
            airMode: true
        });

    });

    window.edit = function() {
            $(".click2edit").summernote()
        },
        window.save = function() {
            $(".click2edit").summernote('destroy');
        }
    </script>
    <script>
    $(document).ready(function() {
        // Basic
        $('.dropify').dropify();

        // Translated
        $('.dropify-fr').dropify({
            messages: {
                default: 'Glissez-déposez un fichier ici ou cliquez',
                replace: 'Glissez-déposez un fichier ou cliquez pour remplacer',
                remove: 'Supprimer',
                error: 'Désolé, le fichier trop volumineux'
            }
        });

        // Used events
        var drEvent = $('#input-file-events').dropify();

        drEvent.on('dropify.beforeClear', function(event, element) {
            return confirm("Do you really want to delete \"" + element.file.name + "\" ?");
        });

        drEvent.on('dropify.afterClear', function(event, element) {
            alert('File deleted');
        });

        drEvent.on('dropify.errors', function(event, element) {
            console.log('Has Errors');
        });

        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function(e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
    </script>
</body>
</html>