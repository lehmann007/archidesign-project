<aside class="left-sidebar">
	<!-- Sidebar scroll-->
	<div class="scroll-sidebar">
		<!-- Sidebar navigation-->
		<nav class="sidebar-nav">
			<ul id="sidebarnav">
				<li class="user-profile"> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><img src="../assets/images/avatar.jpg" alt="user" /><span class="hide-menu"><?php echo $_SESSION["idAdmin"]->prenom ?> <?php echo $_SESSION["idAdmin"]->nom ?> </span></a>
					<ul aria-expanded="false" class="collapse">
						<li><a href="profil.php">Mon Profil </a></li>
						<li><a href="../inc/deconnexion.php">Logout</a></li>
					</ul>
				</li>
				<li class="nav-devider"></li>
				
				<li class="nav-small-cap">GENERAL</li>
				<li> <a class="waves-effect waves-dark" href="index.php" aria-expanded="false"><i class="mdi mdi-gauge"></i><span class="hide-menu">Pr&eacute;sentation accueil</span></a>
				</li>
				
				<li class="nav-small-cap">INFORMATION PAGE</li>
				
				<li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-book-open-variant"></i><span class="hide-menu">Inspirations</span></a>
					<?php $inspirations = findInspiration(); ?>
					<ul aria-expanded="false" class="collapse">
						<?php foreach($inspirations AS $inspiration){ ?>
						<li> <a class="has-arrow" href="#" aria-expanded="false"><?php echo $inspiration->inspiration; ?></a>
							<ul aria-expanded="false" class="collapse">
								<li><a href="inspirationPresentation.php?id=<?php echo $inspiration->id; ?>">Presentation</a></li>
								<li><a href="inspirationPage.php?id=<?php echo $inspiration->id; ?>">Page</a></li>
							</ul>
						</li>
						<?php } ?>
					</ul>
				</li>
				
				<li> <a class="has-arrow waves-effect waves-dark" href="#" aria-expanded="false"><i class="mdi mdi-book-multiple"></i><span class="hide-menu">Pieces</span></a>
					<?php $pieces = findPiece(); ?>
					<ul aria-expanded="false" class="collapse">
						<?php foreach($pieces AS $piece){ ?>
						<li> <a class="has-arrow" href="#" aria-expanded="false"><?php echo $piece->piece; ?></a>
							<ul aria-expanded="false" class="collapse">
								<li><a href="piecePresentation.php?id=<?php echo $piece->id; ?>">Presentation</a></li>
								<li><a href="piecePage.php?id=<?php echo $piece->id; ?>">Page</a></li>
							</ul>
						</li>
						<?php } ?>
					</ul>
				</li>
				
				<li> <a class="waves-effect waves-dark" href="couleur-et-eclairage.php" aria-expanded="false"><i class="mdi mdi-arrange-send-backward"></i><span class="hide-menu">Couleurs et Eclairages</span></a>
				</li>
				
				<li class="nav-small-cap">CONTACT</li>
				<li> <a class="waves-effect waves-dark" href="contact.php" aria-expanded="false"><i class="ti-email"></i><span class="hide-menu">Contact / Localisation</span></a>
				</li>
			</ul>
		</nav>
		<!-- End Sidebar navigation -->
	</div>
	<!-- End Sidebar scroll-->
</aside>