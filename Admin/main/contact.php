<?php
	include('../inc/verificationSession.php');
	require('../inc/connectpdo.php');
	require('../inc/fonction.php');
?>
<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from wrappixel.com/demos/admin-templates/admin-pro/main/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 03 Apr 2018 21:24:30 GMT -->
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <!-- Tell the browser to be responsive to screen width -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- Favicon icon -->
    
    <title>Contact | Admin Archi-Design</title>
	
	<?php include ('../inc/head.php') ?>
</head>

<body class="fix-header fix-sidebar card-no-border">
    <!-- ============================================================== -->
    <!-- Preloader - style you can find in spinners.css -->
    <!-- ============================================================== -->
    <div class="preloader">
        <div class="loader">
            <div class="loader__figure"></div>
            <p class="loader__label">Admin Archi-Design</p>
        </div>
    </div>
    <!-- ============================================================== -->
    <!-- Main wrapper - style you can find in pages.scss -->
    <!-- ============================================================== -->
    <div id="main-wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <header class="topbar">
            <?php include ('nav.php') ?>
        </header>
        <!-- ============================================================== -->
        <!-- End Topbar header -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <?php include ('aside.php') ?>
        <!-- ============================================================== -->
        <!-- End Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <!-- ============================================================== -->
        <!-- Page wrapper  -->
        <!-- ============================================================== -->
        <div class="page-wrapper">
            <!-- ============================================================== -->
            <!-- Container fluid  -->
            <!-- ============================================================== -->
            <div class="container-fluid">
                <!-- ============================================================== -->
                <!-- Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <div class="row page-titles">
                    <div class="col-md-5 align-self-center">
                        <h1 class="text-themecolor">Contact</h1>
                    </div>
                    <div class="col-md-7 align-self-center">
                        <ol class="breadcrumb">
                            <li class="breadcrumb-item"><a href="javascript:void(0)">Contact</a></li>
                        </ol>
                    </div>
                </div>
                <!-- ============================================================== -->
                <!-- End Bread crumb and right sidebar toggle -->
                <!-- ============================================================== -->
                <!-- ============================================================== -->
                <!-- Stats box -->
                <!-- ============================================================== -->
				
				
				
                <div class="row">
					
					<?php $contactInfos = findContactInfo(); ?>
                    <div class="col-lg-3 col-md-3">
                        <div class="card">
                            <div class="card-body">
                                <h2 class="card-title text-center">CONTACT INFORMATION 1</h2>
                                <table class="table vm font-14">
                                    <tr>
                                        <td>NOM : <?php echo $contactInfos[0]->nom; ?></td>
                                    </tr>
									<tr>
                                        <td>RUE : <?php echo $contactInfos[0]->rue; ?></td>
                                    </tr>
                                    <tr>
                                        <td>LIEU : <?php echo $contactInfos[0]->lieu; ?></td>
                                    </tr>
                                    <tr>
                                        <td>CONTACT : <?php echo $contactInfos[0]->phone; ?></td>
                                    </tr>
									<tr>
                                        <td>EMAIL : <?php echo $contactInfos[0]->email; ?></td>
                                    </tr>
                                </table>
								<div class="card-body text-center">
									<a href="contactInfoModification.php?id=<?php echo $contactInfos[0]->id; ?>" class="waves-effect waves-dark btn btn-info btn-md btn-rounded">Modifier</a>
								</div>
                            </div>
                        </div>
                    </div>
					
					<div class="col-lg-3 col-md-3">
                        <div class="card">
                            <div class="card-body">
                                <h2 class="card-title text-center">CONTACT INFORMATION 2</h2>
                                <table class="table vm font-14">
                                    <tr>
                                        <td>NOM : <?php echo $contactInfos[1]->nom; ?></td>
                                    </tr>
									<tr>
                                        <td>RUE : <?php echo $contactInfos[1]->rue; ?></td>
                                    </tr>
                                    <tr>
                                        <td>LIEU : <?php echo $contactInfos[1]->lieu; ?></td>
                                    </tr>
                                    <tr>
                                        <td>CONTACT : <?php echo $contactInfos[1]->phone; ?></td>
                                    </tr>
									<tr>
                                        <td>EMAIL : <?php echo $contactInfos[1]->email; ?></td>
                                    </tr>
                                </table>
								<div class="card-body text-center">
									<a href="contactInfoModification.php?id=<?php echo $contactInfos[1]->id; ?>" class="waves-effect waves-dark btn btn-info btn-md btn-rounded">Modifier</a>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php include ('footer.php') ?>
        </div>
    </div>
	<?php include ('../inc/javascript.php') ?>
</body>
</html>