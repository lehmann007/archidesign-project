<link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
<link href="../assets/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet">
<link href="../assets/plugins/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
<!-- This page CSS -->
<!-- chartist CSS -->
<link href="../assets/plugins/chartist-js/dist/chartist.min.css" rel="stylesheet">
<link href="../assets/plugins/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.css" rel="stylesheet">
<!--c3 CSS -->
<link href="../assets/plugins/c3-master/c3.min.css" rel="stylesheet">
<!--Toaster Popup message CSS -->
<link href="../assets/plugins/toast-master/css/jquery.toast.css" rel="stylesheet">
<!-- Custom CSS -->
<link href="css/style.css" rel="stylesheet">
<!-- Dashboard 1 Page CSS -->
<link href="css/pages/dashboard1.css" rel="stylesheet">
<!-- You can change the theme colors from here -->
<link href="css/colors/default-dark.css" id="theme" rel="stylesheet">