<?php
	require('connectpdo.php');
	require('fonction.php');
	
	$id=$_POST["id"];
	$idPiece=$_POST["idPiece"];
	$descriptionImage=$_POST["descriptionImage"];
	$information=$_POST["information"];
	$paragraphe=$_POST["paragraphe"];
	
	$dossier = '../../archidesign/assets/allImages/';
    $image = $dossier . basename($_FILES['image']['name']);
    if (!empty($_FILES['image'])) {
        if ($_FILES['image']['error'] <= 0) {
            if ($_FILES['image']['size'] <= 2097152) {
                $ImageNews = $_FILES['image']['name'];
                $image = $dossier . basename($_FILES['image']['name']);
                $fichier = basename($_FILES['image']['name']);
                if (move_uploaded_file($_FILES['image']['tmp_name'], $image)) {
                    $picture = "../../archidesign/assets/allImages/" . $_FILES['image']['name'];
                    $size = getimagesize($picture);
                    $NouvelleLargeur = 1920;
                    $NouvelleHauteur = 1280;
                    $miniature = ImageCreateTrueColor($NouvelleLargeur, $NouvelleHauteur);
                    $images = ImageCreateFromJpeg($dossier . $fichier);
                    ImageCopyResampled($miniature, $images, 0, 0, 0, 0, $NouvelleLargeur, $NouvelleHauteur, $size[0], $size[1]);
                    ImageJpeg($miniature, $dossier . $fichier, 100);
                    $imagess = basename($_FILES['image']['name']);
					
					updatePiecePageInformation($id, $descriptionImage, $information, $paragraphe, $imagess);
                }
            } else {
                echo 'Erreur';
            }
        }
		else {
			echo 'Erreur';
		}
    }
	else {
		echo 'Erreur';
	}
	
	?>
		
	<SCRIPT LANGUAGE="JavaScript">
		document.location.href="../main/piecePage.php?id=<?php echo $idPiece ?>"
	</SCRIPT>
		 
	<?php
?>