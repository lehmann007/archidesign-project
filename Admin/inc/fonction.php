<?php
	/* -----------------------------------GENERAL------------------------------------------ */
	
	/* Find ------------------------------------------------------------ */

	function findNameProject(){
		try{
			$connexion = getmysql();
			/*$requete="select*from utilisateur where Login='%s'";
			$requete=sprintf($requete, $_GET["nom"]);*/
			$request="select*from nameProject";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function findMenu(){
		try{
			$connexion = getmysql();
			$request="select*from menu";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findSlide(){
		try{
			$connexion = getmysql();
			$request="select*from slide";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findTitle(){
		try{
			$connexion = getmysql();
			$request="select*from title";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	/* Update ------------------------------------------------------------ */
	
	function updateTitle($titre, $description){
        try{
			$connexion = getmysql();
			$stmt = $connexion->prepare("update title set title=:title, description=:description where id=1");
			$stmt->bindParam(':title', $titre);
			$stmt->bindParam(':description', $description);
			$stmt->execute();
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
		$connexion = null;
    }
	
	
	
	
	/* -----------------------------------INSPIRATION------------------------------------------ */
	
	/* Find ------------------------------------------------------------ */
	
	function findInspiration(){
		try{
			$connexion = getmysql();
			$request="select*from inspiration";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findInspirationAccueil(){
		try{
			$connexion = getmysql();
			$request="select*from inspirationAccueil";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findInspirationAccueilById($idInspiration){
		try{
			$connexion = getmysql();
			$request="select*from inspirationAccueil where idInspiration='%s'";
			$request=sprintf($request, $idInspiration);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function findInspirationPage($idInspiration){
		try{
			$connexion = getmysql();
			$request="select*from inspirationPage where idInspiration='%s'";
			$request=sprintf($request, $idInspiration);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function findInspirationById($idInspiration){
		try{
			$connexion = getmysql();
			$request="select*from inspiration where id='%s'";
			$request=sprintf($request, $idInspiration);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function updateTitleInspiration($inspiration, $id){
        try{
			$connexion = getmysql();
			$stmt = $connexion->prepare("update inspiration set inspiration=:inspiration where id=:id");
			$stmt->bindParam(':inspiration', $inspiration);
			$stmt->bindParam(':id', $id);
			$stmt->execute();
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
		$connexion = null;
    }
	
	function updateInspirationPresentation($image, $descriptionImage, $titre, $presentation, $id){
        try{
			$connexion = getmysql();
			$stmt = $connexion->prepare("update inspirationAccueil set imageAccueil=:image, descriptionImageAccueil=:descriptionImage, inspiration=:titre, presentation=:presentation where idInspiration=:id");
			$stmt->bindParam(':image', $image);
			$stmt->bindParam(':descriptionImage', $descriptionImage);
			$stmt->bindParam(':titre', $titre);
			$stmt->bindParam(':presentation', $presentation);
			$stmt->bindParam(':id', $id);
			$stmt->execute();
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
		$connexion = null;
    }
	
	function updateInspirationPage($id, $descriptionImage1, $titre, $introduction, $paragraphe, $descriptionImage2, $descriptionImage3, $image1, $image2, $image3){
        try{
			$connexion = getmysql();
			$stmt = $connexion->prepare("update inspirationPage set descriptionImage1=:descriptionImage1, title=:titre, paragraphe1=:introduction, paragraphe2=:paragraphe, descriptionImage2=:descriptionImage2, descriptionImage3=:descriptionImage3, image1=:image1, image2=:image2, image3=:image3 where idInspiration=:id");
			$stmt->bindParam(':id', $id);
			$stmt->bindParam(':descriptionImage1', $descriptionImage1);
			$stmt->bindParam(':titre', $titre);
			$stmt->bindParam(':introduction', $introduction);
			$stmt->bindParam(':paragraphe', $paragraphe);
			$stmt->bindParam(':descriptionImage2', $descriptionImage2);
			$stmt->bindParam(':descriptionImage3', $descriptionImage3);
			$stmt->bindParam(':image1', $image1);
			$stmt->bindParam(':image2', $image2);
			$stmt->bindParam(':image3', $image3);
			$stmt->execute();
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
		$connexion = null;
    }
	
	
	
	
	/* -----------------------------------PIECE------------------------------------------ */
	
	/* Find ------------------------------------------------------------ */
	
	function findPiece(){
		try{
			$connexion = getmysql();
			$request="select*from piece";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findPieceAccueil(){
		try{
			$connexion = getmysql();
			$request="select*from pieceAccueil";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findPieceAccueilById($idPiece){
		try{
			$connexion = getmysql();
			$request="select*from pieceAccueil where idPiece='%s'";
			$request=sprintf($request, $idPiece);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function findPiecePage($idPiece){
		try{
			$connexion = getmysql();
			$request="select*from piecePage where idPiece='%s'";
			$request=sprintf($request, $idPiece);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function findPieceById($idPiece){
		try{
			$connexion = getmysql();
			$request="select*from piece where id='%s'";
			$request=sprintf($request, $idPiece);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function findPieceInformation($idPiece){
		try{
			$connexion = getmysql();
			$request="select*from pieceInformation where idPiece='%s'";
			$request=sprintf($request, $idPiece);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findPieceInformationById($id){
		try{
			$connexion = getmysql();
			$request="select*from pieceInformation where ID='%s'";
			$request=sprintf($request, $id);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function updateTitlePiece($piece, $id){
        try{
			$connexion = getmysql();
			$stmt = $connexion->prepare("update piece set piece=:piece where id=:id");
			$stmt->bindParam(':piece', $piece);
			$stmt->bindParam(':id', $id);
			$stmt->execute();
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
		$connexion = null;
    }
	
	function updatePiecePresentation($id, $descriptionBackImage, $titreBloc, $description, $titre, $presentation, $descriptionImage1, $descriptionImage2, $descriptionImage3, $imageBack, $image1, $image2, $image3){
        try{
			$connexion = getmysql();
			$stmt = $connexion->prepare("update pieceAccueil set descriptionBackImage=:descriptionBackImage, piece=:titreBloc, smallDescription=:description, title=:titre, presentation=:presentation, descriptionImage1=:descriptionImage1, descriptionImage2=:descriptionImage2, descriptionImage3=:descriptionImage3, backImage=:imageBack, image1=:image1, image2=:image2, image3=:image3 where idPiece=:id");
			$stmt->bindParam(':id', $id);
			$stmt->bindParam(':descriptionBackImage', $descriptionBackImage);
			$stmt->bindParam(':titreBloc', $titreBloc);
			$stmt->bindParam(':description', $description);
			$stmt->bindParam(':titre', $titre);
			$stmt->bindParam(':presentation', $presentation);
			$stmt->bindParam(':descriptionImage1', $descriptionImage1);
			$stmt->bindParam(':descriptionImage2', $descriptionImage2);
			$stmt->bindParam(':descriptionImage3', $descriptionImage3);
			$stmt->bindParam(':imageBack', $imageBack);
			$stmt->bindParam(':image1', $image1);
			$stmt->bindParam(':image2', $image2);
			$stmt->bindParam(':image3', $image3);
			$stmt->execute();
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
		$connexion = null;
    }
	
	function updatePiecePage($id, $sousTitre, $presentation){
        try{
			$connexion = getmysql();
			$stmt = $connexion->prepare("update piecePage set title=:sousTitre, presentation=:presentation where idPiece=:id");
			$stmt->bindParam(':sousTitre', $sousTitre);
			$stmt->bindParam(':presentation', $presentation);
			$stmt->bindParam(':id', $id);
			$stmt->execute();
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
		$connexion = null;
    }
	
	function updatePiecePageInformation($id, $descriptionImage, $information, $paragraphe, $image){
        try{
			$connexion = getmysql();
			$stmt = $connexion->prepare("update pieceInformation set information=:information, image=:image, descriptionImage=:descriptionImage, paragraphe=:paragraphe where id=:id");
			$stmt->bindParam(':descriptionImage', $descriptionImage);
			$stmt->bindParam(':information', $information);
			$stmt->bindParam(':paragraphe', $paragraphe);
			$stmt->bindParam(':image', $image);
			$stmt->bindParam(':id', $id);
			$stmt->execute();
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
		$connexion = null;
    }
	
	
	
	
	/* -----------------------------------COULEUR ET ECLAIRAGE------------------------------------------ */
	
	/* Find ------------------------------------------------------------ */
	
	function findCouleur(){
		try{
			$connexion = getmysql();
			$request="select*from couleur";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function findEclairage(){
		try{
			$connexion = getmysql();
			$request="select*from eclairage";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function updateCouleur($image1, $descriptionImage, $titre, $description, $paragraphe){
        try{
			$connexion = getmysql();
			$stmt = $connexion->prepare("update couleur set title=:title, description=:description, paragraphe=:paragraphe, image='$image1', descriptionImage=:descriptionImage where id=1");
			$stmt->bindParam(':title', $titre);
			$stmt->bindParam(':description', $description);
			$stmt->bindParam(':paragraphe', $paragraphe);
			//$stmt->bindParam(':image', $image1);
			$stmt->bindParam(':descriptionImage', $descriptionImage);
			$stmt->execute();
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
		$connexion = null;
    }
	
	function updateEclairage($image1, $descriptionImage, $titre, $description, $paragraphe){
        try{
			$connexion = getmysql();
			$stmt = $connexion->prepare("update eclairage set title=:title, description=:description, paragraphe=:paragraphe, image='$image1', descriptionImage=:descriptionImage where id=1");
			$stmt->bindParam(':title', $titre);
			$stmt->bindParam(':description', $description);
			$stmt->bindParam(':paragraphe', $paragraphe);
			//$stmt->bindParam(':image', $image1);
			$stmt->bindParam(':descriptionImage', $descriptionImage);
			$stmt->execute();
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
		$connexion = null;
    }
	
	
	
	
	
	/* -----------------------------------CONTACT------------------------------------------ */
	
	/* Find ------------------------------------------------------------ */
	
	function findContactInfo(){
		try{
			$connexion = getmysql();
			$request="select*from contactInfo";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findContactInfoById($id){
		try{
			$connexion = getmysql();
			$request="select*from contactInfo where id='%s'";
			$request=sprintf($request, $id);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function updateContact($nom, $rue, $lieu, $phone, $email, $id){
        try{
			$connexion = getmysql();
			$stmt = $connexion->prepare("update contactInfo set nom=:nom, rue=:rue, lieu=:lieu, phone=:phone, email=:email where id=:id");
			$stmt->bindParam(':nom', $nom);
			$stmt->bindParam(':rue', $rue);
			$stmt->bindParam(':lieu', $lieu);
			$stmt->bindParam(':phone', $phone);
			$stmt->bindParam(':email', $email);
			$stmt->bindParam(':id', $id);
			$stmt->execute();
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
		$connexion = null;
    }
	
	
	
	/* -----------------------------------BACKOFFICE USER------------------------------------------ */
	
	/* Find ------------------------------------------------------------ */
	
	function connexionAdmin($email, $password){
		try{
			$connexion = getmysql();
			$request="select*from users where email='%s' and password=sha1('%s')";
			$request=sprintf($request, $email, $password);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
?>













