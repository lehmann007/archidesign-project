-- phpMyAdmin SQL Dump
-- version 4.0.2
-- http://www.phpmyadmin.net
--
-- Client: localhost
-- Généré le: Sam 07 Avril 2018 à 10:53
-- Version du serveur: 5.6.11-log
-- Version de PHP: 5.3.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `archidesign`
--
CREATE DATABASE IF NOT EXISTS `archidesign` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `archidesign`;

-- --------------------------------------------------------

--
-- Structure de la table `contactinfo`
--

CREATE TABLE IF NOT EXISTS `contactinfo` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(35) DEFAULT NULL,
  `rue` varchar(35) DEFAULT NULL,
  `lieu` varchar(35) DEFAULT NULL,
  `phone` varchar(35) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `contactinfo`
--

INSERT INTO `contactinfo` (`id`, `nom`, `rue`, `lieu`, `phone`, `email`) VALUES(1, 'Lot IV 5 42 Bis', 'Rue Naka Rabemabantsoa', 'Antanimena', '+261 32 32 332 23', 'archidesign@gmail.com');
INSERT INTO `contactinfo` (`id`, `nom`, `rue`, `lieu`, `phone`, `email`) VALUES(2, 'II B 41', 'Rue Marc Rabibisoa', 'Ampefiloha', '+261 33 23 223 32', 'archidesign@gmail.com');

-- --------------------------------------------------------

--
-- Structure de la table `couleur`
--

CREATE TABLE IF NOT EXISTS `couleur` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(35) DEFAULT NULL,
  `description` text,
  `paragraphe` text,
  `image` varchar(80) DEFAULT NULL,
  `descriptionImage` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `couleur`
--

INSERT INTO `couleur` (`id`, `title`, `description`, `paragraphe`, `image`, `descriptionImage`) VALUES(1, 'Couleur et théorie', 'La couleur est probablement l?élément graphique qui a le plus d?impact sur l?inconscient en matière de design. Elle peut transmettre une émotion, attirer l?attention et même envoyer un message avant même de prendre connaissance du contenu.', 'Malgré l?aspect très subjectif et émotionnel de la couleur, la théorie derrière ces choix est bien réelle et scientifique. De plus, la façon dont la couleur est créée aura un impact sur le résultat final. Pour simplifier, la couleur est soit composée de pixels lumineux rouges, verts et bleus (écrans) ou de pigments d?encre cyan, magenta, jaune et noir (imprimerie). Ce sont les recettes Red Green Blue (RGB) et Cyan Magenta Jaune Noir (CMYK) Il est aussi possible de choisir des couleurs Pantone dont chacune est numérotée et correspond à une couleur précise et unique. La lumière ambiante (incandescent, fluorescent, naturelle, ?) de même que l?environnement autour de la couleur vont influencer la couleur perçue.\nLe spectre des couleurs est presque infini et il est possible de décliner chacune des couleurs en teintes, nuances et variantes. Le choix d?une combinaison de couleurs (palette de couleurs) permet donc d?agencer un petit groupe de couleurs qui se marient bien ensemble. Selon l?effet désiré, plusieurs combinaisons sont possibles : complémentaires, triade, monochromatique, analogue, ? ', 'Image', 'couleur-architecture-design');

-- --------------------------------------------------------

--
-- Structure de la table `eclairage`
--

CREATE TABLE IF NOT EXISTS `eclairage` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(35) DEFAULT NULL,
  `description` text,
  `paragraphe` text,
  `image` varchar(80) DEFAULT NULL,
  `descriptionImage` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `eclairage`
--

INSERT INTO `eclairage` (`id`, `title`, `description`, `paragraphe`, `image`, `descriptionImage`) VALUES(1, 'La lumière', 'Dans une décoration intérieure, l?éclairage est primordial. L''éclairage doit mettre en valeur un objet, une texture ou une zone de la pièce. Pour cela, multiplier les sources lumineuses pour créer des zones d''ombre et de lumière attirant le regard.', 'Éclairage et couleurs\nLes lumières contribuent à créer une ambiance. Les couleurs jouent sur l''ambiance et sur la lumière. Des lumières jaunes (ampoule à incandescence) ou rouges produiront une sensation de chaleur. Des ampoules blanches (halogènes) ou légèrement bleutées donneront une lumière éclatante et une sensation de froid. Si les murs sont de couleur claire, ils réfléchiront la lumière. S''ils sont sombres, ils l''absorberont. Outre l''éclairage d''ambiance, des spots dirigés sur des endroits précis ou des ampoules colorées créeront une atmosphère particulière et personnelle.\nÉclairage direct\nProjetés sur une surface définie, les rayons mettent en valeur un objet ou une zone. Cet éclairage ponctuel diffuse une lumière vive. Plus la lampe est éloignée, plus la zone éclairée est vaste mais moins sl''éclairement est intense. Inconvénient : l''éclairage direct est puissant mais est éblouissant et crée des ombres. L''éclairage peut aussi être décoratif lorsqu''il s''agit de sculpture lumineuse ou de bougie créant ainsi une atmosphère et rendant la pièce plus chaleureuse.\nÉclairage indirect\nCe type d''éclairage crée l''ambiance de la pièce. Des lampes dirigées vers une surface plane, telle un mur ou un plafond, illumineront toute la pièce. Plus la surface est claire et brillante, plus elle réfléchit la lumière. Si la surface est laquée, elle peut éblouir. Pour que la lumière soit d''autant plus diffuse, certains matériaux la rendent plus homogène : verre dépoli, plastique translucide, tissu fin, papier, qui suppriment la brillance de l''ampoule et ne réduisent pas la puissance d''éclairement.', 'Image', 'eclairage-architecture-design');

-- --------------------------------------------------------

--
-- Structure de la table `inspiration`
--

CREATE TABLE IF NOT EXISTS `inspiration` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `inspiration` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `inspiration`
--

INSERT INTO `inspiration` (`id`, `inspiration`) VALUES(1, 'Scandinave');
INSERT INTO `inspiration` (`id`, `inspiration`) VALUES(2, 'Industrielle');
INSERT INTO `inspiration` (`id`, `inspiration`) VALUES(3, 'Exotique');

-- --------------------------------------------------------

--
-- Structure de la table `inspirationaccueil`
--

CREATE TABLE IF NOT EXISTS `inspirationaccueil` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `idInspiration` tinyint(3) unsigned DEFAULT NULL,
  `inspiration` varchar(30) DEFAULT NULL,
  `presentation` text,
  `imageAccueil` varchar(80) DEFAULT NULL,
  `descriptionImageAccueil` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idInspiration` (`idInspiration`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `inspirationaccueil`
--

INSERT INTO `inspirationaccueil` (`id`, `idInspiration`, `inspiration`, `presentation`, `imageAccueil`, `descriptionImageAccueil`) VALUES(1, 1, 'Scandinave', 'Simplicité et fonctionnalité sont deux critères essentiels qui définissent cette décoration confortable venue du froid. A appliquer dans votre intérieur sans modération.', 'Image', 'scandinave-image-inspiration');
INSERT INTO `inspirationaccueil` (`id`, `idInspiration`, `inspiration`, `presentation`, `imageAccueil`, `descriptionImageAccueil`) VALUES(2, 2, 'Industrielle', 'Une tendance très en vogue qui se veut à la fois froide et cosy. La décoration industrielle est l?art de mélanger le vintage et la modernité, à l?image d?une véritable usine.', 'Image', 'industrielle-image-inspiration');
INSERT INTO `inspirationaccueil` (`id`, `idInspiration`, `inspiration`, `presentation`, `imageAccueil`, `descriptionImageAccueil`) VALUES(3, 3, 'Exotique', 'Véritable invitation au voyage, le thème exotique apporte de l?allure et du style à votre intérieur. Des couleurs puissantes et marquées donneront chaleur et convivialité.', 'Image', 'exotique-image-inspiration');

-- --------------------------------------------------------

--
-- Structure de la table `inspirationpage`
--

CREATE TABLE IF NOT EXISTS `inspirationpage` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `idInspiration` tinyint(3) unsigned DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `paragraphe1` text,
  `image1` varchar(80) DEFAULT NULL,
  `descriptionImage1` varchar(1000) DEFAULT NULL,
  `paragraphe2` text,
  `image2` varchar(80) DEFAULT NULL,
  `descriptionImage2` varchar(1000) DEFAULT NULL,
  `image3` varchar(80) DEFAULT NULL,
  `descriptionImage3` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idInspiration` (`idInspiration`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `inspirationpage`
--

INSERT INTO `inspirationpage` (`id`, `idInspiration`, `title`, `paragraphe1`, `image1`, `descriptionImage1`, `paragraphe2`, `image2`, `descriptionImage2`, `image3`, `descriptionImage3`) VALUES(1, 1, 'Decryptage de l indémodable !', 'Cela n aura echappe a personne : nos intérieurs en pincent sévèrement pour le style scandinave !  Épuré, rassurant, beau et fonctionnel, le Nordic way of life a tout pour plaire. Or sous ses allures cool et ultra tendance, il est soumis à quelques règles importantes. Le style a été décrypté dans ses moindres détails pour vous aider à faire un sans-faute dans sa reproduction. Plus que jamais le style scandinave a le sens du beau et du pratique. Il se définit avant tout par son esprit tour à tour nature et élégant, sage mais pas neutre, un ensemble de meuble, petit déco, couleurs qui associés ensemble aspirent à la détente et au bien-être. Se sentir bien chez soi et constituer des pièces cosy et confortables, tel est le crédo du style scandinave. Chaleureux, accueillant, ce style 100% nordique révèle une atmosphère douce e sereine où l?on aime passer du temps.', 'Image1', 'presentation-scandinave-inspiration', 'La lumière joue un rôle primordial dans la maison scandinave. En raison du coucher du soleil très tôt dans les pays scandinaves, multiplier les luminaires chez soi ne serait pas du refus. Ainsi les luminaires sous toutes leurs formes s?exposent dans un intérieur scandinave. On accumule les petites lampes à poser comme les lampadaires ou plafonniers. Enfin, surtout on adopte l?accessoire phare : la bougie. Idem pour les textiles. Plaids, rideaux, coussins : le style scandinave les adore parce qu''ils font échos à cette sensation d''être en sécurité dans une atmosphère douillette.\nCe style nordique se reconnaît par le choix des couleurs et des matériaux. Côté couleurs, on s''oriente vers l?incontournable du style : le blanc, que l''on déclinera sous toutes ses nuances, allant du blanc immaculé au blanc écru en passant par le blanc bleuté et le blanc tirant sur le beige. Pour rythmer le tout, on adopte une ou deux couleurs supplémentaires, toujours naturelles ou neutres ou dans un ton pastel. Ainsi, on choisit le gris perle pour la douceur, le gris anthracite pour donner le ton, du taupe pour réchauffer, du marron glacé pour envelopper. L''idée est de créer un camaïeu de couleurs qui mixe entre couleurs froides et couleurs chaudes dans le but de proposer un intérieur scandinave à l''esprit cocooning.\nPour les matériaux, comme le style scandinave fait directement référence à la nature, le bois est le matériau le plus utilisé. Au sol, pour accentuer le total look blanc on opte pour un parquet blanchi à la scandinave ou on laisse le parquet brut qu''on recouvre d''un tapis douillet. Sur les murs, le soubassement en bois est le bienvenu pour rythmer l''espace avec simplicité. Le bois prend aussi place sur le mobilier et sur les objets. On aime le mixer avec des matières plus nobles afin de créer un contraste subtil. Ainsi, le cuir, le cuivre, le marbre et le métal prennent place avec style, de préférence par l''intermédiaire des objets de décoration et des luminaires.\n', 'Image2', 'image-scandinave', 'Image3', 'scandinave-inspiration');
INSERT INTO `inspirationpage` (`id`, `idInspiration`, `title`, `paragraphe1`, `image1`, `descriptionImage1`, `paragraphe2`, `image2`, `descriptionImage2`, `image3`, `descriptionImage3`) VALUES(2, 2, 'Réussir son style Factory !', 'Savant mélange de meubles destinés au travail ouvrier et de matériaux bruts, le style industriel - malgré son grand âge - n?a pas pris une ride et a toujours la cote dans nos intérieurs. Plus seulement réservé au loft citadin, il s?invite dans nos appartements et maisons de campagne. Le mode d?emploi pour un déco industrielle au top vous y est révélé pour appliquer ce style si particulier. Mur de briques, tuyaux de plomberie et plancher de béton sont des éléments typiques de ce de décoration intérieure. Véritable rappel de l?époque de l?industrialisation et des usines, le style industriel se définit par un mobilier robuste aux lignes épurées sans fioritures ni ornements inutiles. La fonctionnalité et la durabilité des meubles est au premier plan.\nOn y trouve aussi un côté vintage, l?usure et le signe du temps étant également recherchés dans le mobilier.\nPar ailleurs, pour un style scandinave-indu, misez sur des teintes de bois plus claires et l?ajout de fausses fourrures pour la décoration, par exemple.', 'Image1', 'presentation-industrielle-inspiration', 'Les matériaux qu?on retrouve le plus souvent dans le style « indus » sont le bois avec un côté patiné, la brique, le métal, le cuir (pour les fauteuils) et le béton (pour les planchers ou les comptoirs). L?ajout de coussins et de tapis, tout comme des plancher de bois viennent réchauffer l?atmosphère des pièces parfois un peu froides dû au caractère brut du mobilier.\nBien que ce style puisse s?appliquer à n?importe quelle pièce, privilégiez les espaces ouverts ainsi que les pièces au plafond haut et aux grandes fenêtres.\n\nGénéralement en bois et e métal, les meubles qu?on retrouve dans le style industriel se caractérisent par leur côté plus massif, en rappel à l?univers ouvrier. Un côté élimé ou vintage est aussi très populaire, comme une peinture écaillée ou un métal légèrement rouillé. Le détournement du mobilier est également fréquent. Ainsi, une échelle métallique pourrait devenir une étagère. Ou bin, une caisse de bois pourrait faire office de table.\n\nLes couleurs du style industriel sont généralement assez sombres : noir, nuances de gris, kaki, rouille, rouge brique, différentes nuances de brun, puis un peu de blanc ou de beige ici et là pour éclairer.\n\nLes accessoires du style industriel sont souvent robustes et imposants. Pensez à un ventilateur métallique sur pied, une vielle machine à écrire, des caisses en métal, une grosse horloge, des objets à poulie, des panneaux anciens de commerce, ?\n\nLa lumière est également essentielle. Posez des spots au plafond, mais aussi des lampes aux allures industrielles ou simplement des ampoules dénudées suspendues.\n', 'Image2', 'image-industrielle', 'Image3', 'industrielle-inspiration');
INSERT INTO `inspirationpage` (`id`, `idInspiration`, `title`, `paragraphe1`, `image1`, `descriptionImage1`, `paragraphe2`, `image2`, `descriptionImage2`, `image3`, `descriptionImage3`) VALUES(3, 3, 'La déco qui voyage?', 'Une des grandes tendances déco, le grand mantra de ce courant : vous dépayser. Tropical, équatorial ou encore asiatique le style exotique va vous transportez loin de chez vous, tout en restant dépayser. Apporter une touche exotique à votre intérieur, c?est la garantie d?une maison qui vous transporte ailleurs toute l?année ! Matières, motifs, inspirations, découvrez les conseils pour adopter le style. Le style exotique est un style plutôt chargé et caractérisé par les déclinaisons de vert, rappel de la nature. Il faudra donc avoir la main plutôt légère et savoir doser sa décoration et la rendre cohérente. \nQui dit parti décoratif fort, dit risque de se lasser. En effet, avec les saisons vous pourriez avoir envie de changement et faire évoluer votre intérieur. La solution : misez sur les accessoires et le non-définitif. Gardez la base de votre déco neutre et accessoirisez avec les housses de canapé ou coussin à motifs, des grands tapis en jonc et des plantes vertes indémodables.', 'Image1', 'presentation-exotique-inspiration', 'Si vous êtes tentés par le style tropical, shoppez les quelques basiques incontournables suivants : de la déco avec des motifs ananas, un fauteuil ou une armoire en bois, une plate exotique avec de grandes feuille et un lé de papier-peint jungle.\nSi vous voulez une touche orientale, parez- vous de couleurs vibrantes, de métal brillant et de matières rassurantes. Meules, tapis, luminaires vont métamorphoser votre cocon en véritable palais des Mille et une nuits !\nSi vous voulez voyager du côté de l?Asie, pensez à des petites statuettes traditionnelles, du bambou, quelques éléments de déco zen ou encore un souvenir de vacances. Tout est dans le dosage.', 'Image2', 'image-exotique', 'Image3', 'exotique-inspiration');

-- --------------------------------------------------------

--
-- Structure de la table `menu`
--

CREATE TABLE IF NOT EXISTS `menu` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `menu` varchar(35) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=6 ;

--
-- Contenu de la table `menu`
--

INSERT INTO `menu` (`id`, `menu`) VALUES(1, 'Home');
INSERT INTO `menu` (`id`, `menu`) VALUES(2, 'Inspiration');
INSERT INTO `menu` (`id`, `menu`) VALUES(3, 'Quelles pieces ?');
INSERT INTO `menu` (`id`, `menu`) VALUES(4, 'Couleurs et eclairages');
INSERT INTO `menu` (`id`, `menu`) VALUES(5, 'Contact');

-- --------------------------------------------------------

--
-- Structure de la table `nameproject`
--

CREATE TABLE IF NOT EXISTS `nameproject` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `nameproject`
--

INSERT INTO `nameproject` (`id`, `name`) VALUES(1, 'ARCHI-DESIGN');

-- --------------------------------------------------------

--
-- Structure de la table `piece`
--

CREATE TABLE IF NOT EXISTS `piece` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `piece` varchar(16) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `piece`
--

INSERT INTO `piece` (`id`, `piece`) VALUES(1, 'Cuisine');
INSERT INTO `piece` (`id`, `piece`) VALUES(2, 'Salon');
INSERT INTO `piece` (`id`, `piece`) VALUES(3, 'Chambre');

-- --------------------------------------------------------

--
-- Structure de la table `pieceaccueil`
--

CREATE TABLE IF NOT EXISTS `pieceaccueil` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `idPiece` tinyint(3) unsigned DEFAULT NULL,
  `piece` varchar(30) DEFAULT NULL,
  `smallDescription` varchar(50) DEFAULT NULL,
  `backImage` varchar(80) DEFAULT NULL,
  `descriptionBackImage` varchar(1000) DEFAULT NULL,
  `title` varchar(50) DEFAULT NULL,
  `presentation` text,
  `image1` varchar(80) DEFAULT NULL,
  `descriptionImage1` varchar(1000) DEFAULT NULL,
  `image2` varchar(80) DEFAULT NULL,
  `descriptionImage2` varchar(1000) DEFAULT NULL,
  `image3` varchar(80) DEFAULT NULL,
  `descriptionImage3` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `idPiece` (`idPiece`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `pieceaccueil`
--

INSERT INTO `pieceaccueil` (`id`, `idPiece`, `piece`, `smallDescription`, `backImage`, `descriptionBackImage`, `title`, `presentation`, `image1`, `descriptionImage1`, `image2`, `descriptionImage2`, `image3`, `descriptionImage3`) VALUES(1, 1, 'Cuisine', 'Fonctionnelle et pratique', 'Image', 'presentation-cuisine-piece', 'Calvaire ou jeu d?enfant', 'L?aménagement d?une cuisine doit être précis et bien pensé pour plus de fonctionnalités. En effet, pour une cuisine ouverte sur le salon ou cuisine fermée, l?agencement, le choix et la disposition des meubles bas et hauts seront différents. Selon sa superficie, sa configuration et selon vos besoins, quelques préceptes sont à respecter. Pour une cuisine aussi pratique qu?esthétique, des règles de base sont nécessaire pour bin l?aménager et disposer ses éléments le plus astucieusement possible.', 'Image', 'image-cuisine', 'Image', 'cuisine-piece', 'Image', 'cuisine-architecture-design');
INSERT INTO `pieceaccueil` (`id`, `idPiece`, `piece`, `smallDescription`, `backImage`, `descriptionBackImage`, `title`, `presentation`, `image1`, `descriptionImage1`, `image2`, `descriptionImage2`, `image3`, `descriptionImage3`) VALUES(2, 2, 'Salon', 'Spatieux et confortable', 'Image', 'presentation-salon-piece', 'Style et chaleureux', 'Le salon est au c?ur de notre vie quotidienne, une pièce aux nombreuses fonctions, un lieu de détente et de repos comme de convivialité où l?on aime passer du temps. Il est donc important de ne pas négliger son aménagement. C?est une des raisons pour laquelle son aménagement est primordial. Aménager un petit salon ou un grand salon n?est pas si évident tout comme choisir et placer les meubles pour une circulation fluide. Pour réussir un agencement sans-faute, quelques points sont à considérer.', 'Image', 'image-salon', 'Image', 'salon-piece', 'Image', 'salon-architecture-design');
INSERT INTO `pieceaccueil` (`id`, `idPiece`, `piece`, `smallDescription`, `backImage`, `descriptionBackImage`, `title`, `presentation`, `image1`, `descriptionImage1`, `image2`, `descriptionImage2`, `image3`, `descriptionImage3`) VALUES(3, 3, 'Chambre', 'Douillet et tendance', 'Image', 'presentation-chambre-piece', 'Une belle chambre pour un sommeil reparateur', 'Chaque pièce de la maison a ses contraintes et ses spécificités. Si réussir son aménagement est une affaire de goût, c?est d?abord une question de logique et d?organisation. La configuration architecturale, l?espace disponible, le profil des habitant, le choix des meubles et leur disposition, il convient de répondre à de multiples critères parfois très personnels. Mais il existe tout de même certaines règles d?agencement élémentaires à suivre pour optimiser chaque pièce, la chambre à coucher y compris.', 'Image', 'image-chambre', 'Image', 'chambre-piece', 'Image', 'chambre-architecture-design');

-- --------------------------------------------------------

--
-- Structure de la table `pieceinformation`
--

CREATE TABLE IF NOT EXISTS `pieceinformation` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `idPiece` tinyint(3) unsigned DEFAULT NULL,
  `information` varchar(75) DEFAULT NULL,
  `image` varchar(80) DEFAULT NULL,
  `descriptionImage` varchar(1000) DEFAULT NULL,
  `paragraphe` text,
  PRIMARY KEY (`id`),
  KEY `idPiece` (`idPiece`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=12 ;

--
-- Contenu de la table `pieceinformation`
--

INSERT INTO `pieceinformation` (`id`, `idPiece`, `information`, `image`, `descriptionImage`, `paragraphe`) VALUES(1, 1, '1.Le triangle d?activité :', 'image', 'presentation-piece-cuisine', 'Plaques de cuisson évier et réfrigérateur, il est important que la distance entre ces trois zones soit égale ou presque, en évitant aussi les trop grandes distances entre ces points. Dans les angles, ne placer ni l?évier (éventuellement l?égouttoir) ni les plaques de cuisson.');
INSERT INTO `pieceinformation` (`id`, `idPiece`, `information`, `image`, `descriptionImage`, `paragraphe`) VALUES(2, 1, '2.Le cheminement du propre et du sale :', 'image', 'presentation-piece-cuisine', 'Penser à la circulation de assiettes et plats sales : dépôt sur le plan travail, passage à la poubelle pour jeter les restes, évier pour éventuellement rincer, puis lave-vaisselle. Ce cheminement doit être simple, rapide et sans encombre. Il en est de même pour le cheminement du propre entre le lave-vaisselle (ou l?égouttoir) et les zones de rangements de la vaisselle propre.');
INSERT INTO `pieceinformation` (`id`, `idPiece`, `information`, `image`, `descriptionImage`, `paragraphe`) VALUES(3, 1, '3.Les hauteurs à respecter :', 'image', 'presentation-piece-cuisine', 'Les hauteurs du plan de travail et de l?électroménager dépendent avant tout de votre taille. Généralement, le plan de travail doit se trouver à la hauteur de la ceinture (entre 85 et 95 cm) ; le four se place de préférence à hauteur des yeux ; une tablette de bar se trouve à la hauteur des coudes (entre 110 et 115cm) ; la hotte aspirante devrait être placée à 60 cm au-dessus des plaques, et les placards de rangement les plus haut doivent se situer à environ 220 cm.');
INSERT INTO `pieceinformation` (`id`, `idPiece`, `information`, `image`, `descriptionImage`, `paragraphe`) VALUES(4, 1, '4.Les mesures à respecter :', 'image', 'presentation-piece-cuisine', '70 cm sont nécessaires à la circulation devant les meubles et électroménagers, mais ce chiffre grimpe à 90 cm s?il s?agit d?un placard ou d?un lave-vaisselle, et 120 cm quand on est plusieurs à circuler. ');
INSERT INTO `pieceinformation` (`id`, `idPiece`, `information`, `image`, `descriptionImage`, `paragraphe`) VALUES(5, 2, '1.Commencer par placer le canapé :', 'image', 'presentation-piece-salon', 'Elément principal, le canapé détermine le reste de l?aménagement du salon. Contre un mur pour un gain de place, il peut aussi être disposé en travers de la pièce comme une séparation entre deux espaces distincts. Son orientation se pense en fonction de l?entrée de la lumière dans la pièce et du point vers lequel vous souhaitez porter l?attention.');
INSERT INTO `pieceinformation` (`id`, `idPiece`, `information`, `image`, `descriptionImage`, `paragraphe`) VALUES(6, 2, '2.Disposer les meubles selon votre quotidien :', 'image', 'presentation-piece-salon', 'Le schéma de la pièce doit être en adéquation avec votre quotidien (le nombre de personne qui s?y côtoient, les habitudes de vies, les espaces libres pour jouer à prévoir pour les enfants), calculer la bonne distance entre votre canapé et votre télé (suivant taille de l?écran).');
INSERT INTO `pieceinformation` (`id`, `idPiece`, `information`, `image`, `descriptionImage`, `paragraphe`) VALUES(7, 2, '3.Soigner la lumière :', 'image', 'presentation-piece-salon', 'La lumière doit être abondante ; pour cela placez le coin canapé près d?une fenêtre pour gagner en luminosité naturelle. Préférer les couleurs claires pour agrandir la sensation d?espace et réfléchir au mieux la luminosité. De plus, multiplier les sources de lumière directe et indirecte pour un salon agréable à vivre.');
INSERT INTO `pieceinformation` (`id`, `idPiece`, `information`, `image`, `descriptionImage`, `paragraphe`) VALUES(8, 2, '4.Opter pour un sol pratique', 'image', 'presentation-piece-salon', 'Le salon est forcément un lieu de passage, le côté pratique est alors à privilégier. Carrelage : réputé pour ses nombreuses qualité, PVC, parquet, sol en fibre naturelle sont aussi de bon choix. A noter : les sols clairs sont plus salissants e demandent plus d?entretien. ');
INSERT INTO `pieceinformation` (`id`, `idPiece`, `information`, `image`, `descriptionImage`, `paragraphe`) VALUES(9, 3, '1.Ne pas négliger la place du lit :', 'image', 'presentation-piece-chambre', 'Pensez bien à la position du lit, de préférence loin de la porte et contre un mur, projetez-vous en se demandant ce que vous voulez voir/entendre.');
INSERT INTO `pieceinformation` (`id`, `idPiece`, `information`, `image`, `descriptionImage`, `paragraphe`) VALUES(10, 3, '2.Pas de bureau dans la chambre :', 'image', 'presentation-piece-chambre', 'Si le bureau n?a pas d?autre place que dans la chambre, essayez de le masquer (exemple dans une bibliothèque, une étagère u peu profonde).');
INSERT INTO `pieceinformation` (`id`, `idPiece`, `information`, `image`, `descriptionImage`, `paragraphe`) VALUES(11, 3, '3.Rester organiser :', 'image', 'presentation-piece-chambre', 'Notez votre scénario quotidien en vous posant les bonnes questions sur vos habitudes. Puis déterminer ce dont vous avez besoin pour une meilleure organisation : un portant pour les vêtements, un placard à chaussures, un beau panier à linge, etc. Les rangements prennent place dans la chambre ou la salle de bain ou le dressing, selon vos habitudes.');

-- --------------------------------------------------------

--
-- Structure de la table `piecepage`
--

CREATE TABLE IF NOT EXISTS `piecepage` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `idPiece` tinyint(3) unsigned DEFAULT NULL,
  `title` varchar(45) DEFAULT NULL,
  `presentation` text,
  PRIMARY KEY (`id`),
  KEY `idPiece` (`idPiece`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `piecepage`
--

INSERT INTO `piecepage` (`id`, `idPiece`, `title`, `presentation`) VALUES(1, 1, 'Du pratique à l?esthétique', 'L?aménagement d?une cuisine obéit à un protocole bien établi. Sens de la circulation, appelé triangle d?activité, distances, rangements et meubles? Découvrez les 5 règles sacrées qui feront de la cuisine un espace fonctionnel, agréable à vivre et à cuisiner.');
INSERT INTO `piecepage` (`id`, `idPiece`, `title`, `presentation`) VALUES(2, 2, 'Presenter le meilleur salon', 'De la place du canapé au choix du sol, voici 4 conseils indispensables pour bien aménager et décorer un salon pratique et convivial.');
INSERT INTO `piecepage` (`id`, `idPiece`, `title`, `presentation`) VALUES(3, 3, 'Amenager parfaitement sa chambre', 'Mauvais sommeil, espace peu pratique, lit mal orienté ? Une chambre mal agencée se ressent au quotidien. C?est pourtant un espace dans lequel nous passons du temps, il convient donc de l?aménager au mieux et d?éviter quelques erreurs.');

-- --------------------------------------------------------

--
-- Structure de la table `slide`
--

CREATE TABLE IF NOT EXISTS `slide` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `smallDescription` varchar(50) DEFAULT NULL,
  `description` varchar(50) DEFAULT NULL,
  `link` varchar(35) DEFAULT NULL,
  `image` varchar(80) DEFAULT NULL,
  `descriptionImage` varchar(1000) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Contenu de la table `slide`
--

INSERT INTO `slide` (`id`, `smallDescription`, `description`, `link`, `image`, `descriptionImage`) VALUES(1, 'Présenter la beauté', 'L''architecture design', 'Inspiration scandinave', 'image', 'presentation-design-salon');
INSERT INTO `slide` (`id`, `smallDescription`, `description`, `link`, `image`, `descriptionImage`) VALUES(2, 'Rénovation', 'Nouveau décor', 'Presentation piece', 'image', 'design-interieur');
INSERT INTO `slide` (`id`, `smallDescription`, `description`, `link`, `image`, `descriptionImage`) VALUES(3, 'Choisir l''ambiance', 'Meilleurs eclairages', 'Ambiance', 'image', 'couleur-ambiance-piece');

-- --------------------------------------------------------

--
-- Structure de la table `title`
--

CREATE TABLE IF NOT EXISTS `title` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) DEFAULT NULL,
  `description` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Contenu de la table `title`
--

INSERT INTO `title` (`id`, `title`, `description`) VALUES(1, 'Des inspirations pour chacune des pieces', 'Piece / Inspiration Decoration / Couleur / Eclairage');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `password` varchar(250) DEFAULT NULL,
  `isAdmin` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `nom`, `prenom`, `email`, `password`, `isAdmin`) VALUES(1, 'Razafimahenina', 'Manoa', 'manoa@gmail.com', '8aca99d9dc94ecef1197ad48c3b16b9ccac82945', 1);
INSERT INTO `users` (`id`, `nom`, `prenom`, `email`, `password`, `isAdmin`) VALUES(2, 'Admin', 'Chef', 'admin@gmail.com', 'dc76e9f0c0006e8f919e0c515c66dbba3982f785', 1);

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `inspirationaccueil`
--
ALTER TABLE `inspirationaccueil`
  ADD CONSTRAINT `inspirationaccueil_ibfk_1` FOREIGN KEY (`idInspiration`) REFERENCES `inspiration` (`id`);

--
-- Contraintes pour la table `inspirationpage`
--
ALTER TABLE `inspirationpage`
  ADD CONSTRAINT `inspirationpage_ibfk_1` FOREIGN KEY (`idInspiration`) REFERENCES `inspiration` (`id`);

--
-- Contraintes pour la table `pieceaccueil`
--
ALTER TABLE `pieceaccueil`
  ADD CONSTRAINT `pieceaccueil_ibfk_1` FOREIGN KEY (`idPiece`) REFERENCES `piece` (`id`);

--
-- Contraintes pour la table `pieceinformation`
--
ALTER TABLE `pieceinformation`
  ADD CONSTRAINT `pieceinformation_ibfk_1` FOREIGN KEY (`idPiece`) REFERENCES `piece` (`id`);

--
-- Contraintes pour la table `piecepage`
--
ALTER TABLE `piecepage`
  ADD CONSTRAINT `piecepage_ibfk_1` FOREIGN KEY (`idPiece`) REFERENCES `piece` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
