<link rel="apple-touch-icon" sizes="180x180" href="assets/images/favicons/apple-touch-icon.png">
<link rel="icon" type="image/png" sizes="32x32" href="assets/images/favicons/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="16x16" href="assets/images/favicons/favicon-16x16.png">
<link rel="shortcut icon" type="image/x-icon" href="assets/images/favicons/favicon.ico">
<link rel="manifest" href="assets/images/favicons/manifest.html">
<link rel="mask-icon" href="assets/images/favicons/safari-pinned-tab.svg" color="#5bbad5">

<link href="assets/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">
<link href="assets/css/style.css" rel="stylesheet">