<!--  -->
<!--    JavaScripts-->
<!--    =============================================-->


<script src="assets/lib/jquery/dist/jquery.min.js"></script>
<script src="assets/lib/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="assets/lib/imagesloaded/imagesloaded.pkgd.min.js"></script>
<script src="assets/lib/jquery-menu-aim/jquery.menu-aim.js"></script>
<script src="assets/lib/gsap/src/minified/TweenMax.min.js"></script>
<script src="assets/lib/gsap/src/minified/plugins/ScrollToPlugin.min.js"></script>
<script src="assets/lib/CustomEase.min.js"></script>
<script src="assets/js/config.js"></script>
<script src="assets/lib/rellax/rellax.min.js"></script>
<script src="assets/js/zanimation.js"></script>
<script src="assets/js/inertia.js"></script>
<script src="assets/lib/modernizr/2.8.3/modernizr.min.js"></script>
<script src="assets/lib/popper.js/1.11.0/umd/popper.min.js" integrity="sha384-b/U6ypiBEHpOf/4+1nzFpr53nxSS+GLCkfwBdFNTxtclqqenISfwAzpKaMNFNmj4"
crossorigin="anonymous"></script>
<!-- Hotjar Tracking Code for http://markup.themewagon.com/tryfreya/-->
<script>(function (h, o, t, j, a, r) {
		h.hj = h.hj || function () { (h.hj.q = h.hj.q || []).push(arguments) };
		h._hjSettings = { hjid: 731669, hjsv: 6 };
		a = o.getElementsByTagName('head')[0];
		r = o.createElement('script'); r.async = 1;
		r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
		a.appendChild(r);
	})(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');

</script>
<!-- Global site tag (gtag.js) - Google Analytics-->
<script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-76729372-6"></script>
<script>window.dataLayer = window.dataLayer || [];
	function gtag() { dataLayer.push(arguments); }
	gtag('js', new Date());
	gtag('config', 'UA-76729372-6');
</script>
<script src="assets/lib/flexslider/jquery.flexslider-min.js"></script>
<script src="assets/js/core.js"></script>
<script src="assets/js/main.js"></script>
<script src="assets/js/modern.js"></script>
<script src="assets/js/popper.js"></script>