<?php
	/* -----------------------------------GENERAL------------------------------------------ */
	
	/* Find ------------------------------------------------------------ */

	function findNameProject(){
		try{
			$connexion = getmysql();
			/*$requete="select*from utilisateur where Login='%s'";
			$requete=sprintf($requete, $_GET["nom"]);*/
			$request="select*from nameProject";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function findMenu(){
		try{
			$connexion = getmysql();
			$request="select*from menu";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findSlide(){
		try{
			$connexion = getmysql();
			$request="select*from slide";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findTitle(){
		try{
			$connexion = getmysql();
			$request="select*from title";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	/*Insert -------------------------------------------------------------- */
	
	function insertCategorie($categorie){
        try{
			$connexion = getmysql();
			$stmt = $conn->prepare("INSERT INTO MyGuests (firstname, lastname, email) VALUES (:firstname, :lastname, :email)");
			$stmt->bindParam(':firstname', $firstname);
			$stmt->bindParam(':lastname', $lastname);
			$stmt->bindParam(':email', $email);
			$stmt->execute();
		}
		catch(Exception $e){
			echo 'Erreur:'.$e->getMessage().'<br />';
			die();
		}
		$connexion = null;
    }
	
	
	
	
	/* -----------------------------------INSPIRATION------------------------------------------ */
	
	/* Find ------------------------------------------------------------ */
	
	function findInspiration(){
		try{
			$connexion = getmysql();
			$request="select*from inspiration";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findInspirationAccueil(){
		try{
			$connexion = getmysql();
			$request="select*from inspirationAccueil";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findInspirationPage($idInspiration){
		try{
			$connexion = getmysql();
			$request="select*from inspirationPage where idInspiration='%s'";
			$request=sprintf($request, $idInspiration);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function findInspirationById($id){
		try{
			$connexion = getmysql();
			$request="select*from inspiration where id='%s'";
			$request=sprintf($request, $id);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	
	
	
	/* -----------------------------------PIECE------------------------------------------ */
	
	/* Find ------------------------------------------------------------ */
	
	function findPiece(){
		try{
			$connexion = getmysql();
			$request="select*from piece";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findPieceAccueil(){
		try{
			$connexion = getmysql();
			$request="select*from pieceAccueil";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findPiecePage($idPiece){
		try{
			$connexion = getmysql();
			$request="select*from piecePage where idPiece='%s'";
			$request=sprintf($request, $idPiece);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function findPieceInformation($idPiece){
		try{
			$connexion = getmysql();
			$request="select*from pieceInformation where idPiece='%s'";
			$request=sprintf($request, $idPiece);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
	function findPieceById($id){
		try{
			$connexion = getmysql();
			$request="select*from piece where id='%s'";
			$request=sprintf($request, $id);
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	
	
	
	/* -----------------------------------COULEUR ET ECLAIRAGE------------------------------------------ */
	
	/* Find ------------------------------------------------------------ */
	
	function findCouleur(){
		try{
			$connexion = getmysql();
			$request="select*from couleur";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	function findEclairage(){
		try{
			$connexion = getmysql();
			$request="select*from eclairage";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result[0];
    }
	
	
	
	
	
	/* -----------------------------------CONTACT------------------------------------------ */
	
	/* Find ------------------------------------------------------------ */
	
	function findContactInfo(){
		try{
			$connexion = getmysql();
			$request="select*from contactInfo";
            $resultats=$connexion->query($request);
            $resultats->setFetchMode(PDO::FETCH_OBJ);
            $result = array();
            while( $ligne = $resultats->fetch() ) {
                $result[] = $ligne;
            }
			$resultats->closeCursor(); 
		}catch(PDOException $e){
		    echo ($e->getMessage());
		}
		$connexion = null;
        return $result;
    }
	
?>













