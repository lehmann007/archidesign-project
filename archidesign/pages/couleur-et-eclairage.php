<?php
	require('../inc/utilitaire.php');
	require('../inc/connectpdo.php');
	require('../inc/fonction.php');
	if($_GET['url'] != ""){
		header('Location: couleurs-et-eclairages');
	}
?>
<!DOCTYPE html>
<html lang="en-US">
<!-- Mirrored from markup.themewagon.com/tryfreya/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Mar 2018 09:25:50 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="couleur, eclairage" />
    <title>Couleur et Eclairage | Archi-Design</title>
	<meta name="description" content="La couleur est probablement l’element graphique qui a le plus d’impact sur l’inconscient en matiere de design. Elle peut transmettre une emotion, attirer l’attention et meme envoyer un message avant meme de prendre connaissance du contenu. Dans une decoration interieure, l’eclairage est primordial. L'eclairage doit mettre en valeur un objet, une texture ou une zone de la piece. Pour cela, multiplier les sources lumineuses pour creer des zones d'ombre et de lumiere attirant le regard." />
    <meta name="theme-color" content="#ffffff">
	
	<link rel="stylesheet" href="inc/concatCSS.php" type="text/css">
	
	<?php include ('../inc/head.php') ?>
</head>

<body data-spy="scroll" data-target=".inner-link" data-offset="60">
    <main>
	
		<!--  Menu -->
		<?php include ('menu.php') ?>
		<!--  EndMenu -->
		
        <section class="py-9 overflow-hidden text-center">
			<div class="background-holder overlay overlay-1 parallax"><img style="margin-top: -160px;" src="assets/images/couleur-et-eclairage.jpg" alt="couleur-et-eclairage" /></div>
            <!--/.background-holder-->
            <div class="container">
                <div class="row" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="col">
                        <div class="overflow-hidden">
                            <h1 class="fs-5 fs-sm-6 color-white mb-3" data-zanim='{"delay":0}'>Couleurs et Eclairages</h1>
                        </div>
                        <div class="overflow-hidden">
                            <h5 class="fs-2 fw-300 ls color-8 text-uppercase" data-zanim='{"delay":0.1}'>Design</h5>
                        </div>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <section class="text-center">
            <div class="container">
                <div class="row justify-content-center">
					<?php $couleur = findCouleur(); ?>
                    <div class="col-lg-8 col-md-10">
                        <h2>
							<span class="d-inline-block color-black"><?php echo $couleur->title; ?></span>
                        </h2>
                        <div class="mb-7">
                            <?php echo $couleur->description; ?>
                        </div>
                        <div class="text-justify">
                            <h5 class="mb-4">
								<?php echo $couleur->paragraphe; ?>
                            </h5>
							<img src="assets/allImages/<?php echo $couleur->image; ?>" alt="<?php echo $couleur->descriptionImage; ?>">
                        </div>
                    </div>
					
					<?php $eclairage = findEclairage(); ?>
                    <div class="col-lg-8 col-md-10 mt-8">
                        <h2>
                            <span class="d-inline-block color-black"><?php echo $eclairage->title; ?></span>
                        </h2>
                        <div class="mb-7">
							<?php echo $eclairage->description; ?>
                        </div>
                        <div class="text-justify">
                            <h5 class="mb-4">
								<?php echo $eclairage->paragraphe; ?>
                            </h5>
                            <img src="assets/allImages/<?php echo $eclairage->image; ?>" alt="<?php echo $eclairage->descriptionImage; ?>">
                        </div>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <!--  Footer -->
		<?php include ('footer.php') ?>
		<!--  EndFooter -->
    </main>
    <!--  -->
    <!--    JavaScripts-->
    <!--    =============================================-->
	<?php include ('../inc/javascript.php') ?>
	
</body>
<!-- Mirrored from markup.themewagon.com/tryfreya/blog.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Mar 2018 09:28:36 GMT -->

</html>