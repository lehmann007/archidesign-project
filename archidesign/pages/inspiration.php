<?php
	require('../inc/utilitaire.php');
	require('../inc/connectpdo.php');
	require('../inc/fonction.php');
	$inspirationPage = findInspirationPage($_GET['id']);
	$inspirationTitre = findInspirationById($_GET['id']);
	if($_GET['url'] != strtolower($inspirationTitre->inspiration)){
		header('Location: '.getUrl($inspirationTitre->inspiration).'-inspiration'.$inspirationTitre->id);
	}
?>
<!DOCTYPE html>
<html lang="en-US">
<!-- Mirrored from markup.themewagon.com/tryfreya/project.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Mar 2018 09:25:50 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="inspiration" />
    <title>Inspiration : <?php echo $inspirationTitre->inspiration ?> | Archi-Design</title>
	<meta name="description" content="Inspiration on decoration peut offrir une bonne decoration pour l'interieur de votre maison. Le but est de reussir votre style" />
    <meta name="theme-color" content="#ffffff">
	
	<link rel="stylesheet" href="inc/concatCSS.php" type="text/css">
	
	<?php include ('../inc/head.php') ?>
</head>

<body data-spy="scroll" data-target=".inner-link" data-offset="60">
    <main>
		
		<!--  Menu -->
		<?php include ('menu.php') ?>
		<!--  EndMenu -->
		
        <section class="py-9 overflow-hidden text-center">
            <div class="background-holder overlay overlay-1 parallax"><img style="margin-top: -160px;" src="assets/images/inspiration-presentation.jpg" alt="inspiration-presentation" /></div>
            <!--/.background-holder-->
            <div class="container">
                <div class="row" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="col">
                        <div class="overflow-hidden">
                            <h1 class="fs-5 fs-sm-6 color-white mb-3" data-zanim='{"delay":0}'>Inspiration : <?php echo $inspirationTitre->inspiration ?></h1>
                        </div>
                        <div class="overflow-hidden">
                            <h5 class="fs-2 fw-300 ls color-8 text-uppercase" data-zanim='{"delay":0.1}'><?php echo $inspirationTitre->inspiration ?></h5>
                        </div>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <section class="text-justify">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <h2 class="mb-4"><?php echo $inspirationPage->title; ?></h2>
                        <h5 class="mb-7"><?php echo $inspirationPage->paragraphe1; ?></h5>
							
						<img src="assets/allImages/<?php echo $inspirationPage->image1; ?>" alt="<?php echo $inspirationPage->descriptionImage1; ?>">
						
                        <h5 class="mt-5"><?php echo $inspirationPage->paragraphe2; ?></h5>
                        <div class="row mt-4">
                            <div class="col-lg-6 mb-4 mb-lg-0">
                                <img class="w-100" src="assets/allImages/<?php echo $inspirationPage->image2; ?>" alt="<?php echo $inspirationPage->descriptionImage2; ?>">
                            </div>
                            <div class="col-lg-6">
                                <img class="w-100" src="assets/allImages/<?php echo $inspirationPage->image3; ?>" alt="<?php echo $inspirationPage->descriptionImage3; ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
		
        <!--  Footer -->
		<?php include ('footer.php') ?>
		<!--  EndFooter -->
		
    </main>
    <!--  -->
    <!--    JavaScripts-->
    <!--    =============================================-->
    <?php include ('../inc/javascript.php') ?>
</body>
<!-- Mirrored from markup.themewagon.com/tryfreya/project.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Mar 2018 09:25:50 GMT -->

</html>