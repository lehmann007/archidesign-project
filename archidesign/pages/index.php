<?php
	require('../inc/utilitaire.php');
	require('../inc/connectpdo.php');
	require('../inc/fonction.php');
	$title = findTitle();
?>
<!DOCTYPE html>
<html lang="en-US">
<!-- Mirrored from markup.themewagon.com/tryfreya/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Mar 2018 09:18:20 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="architecture, decor, design, inspiration, piece" />
    <title><?php echo $title->title; ?> | Archi-Design</title>
	<meta name="description" content="L'architecture design fait parti des elements cle pour une bonne visualisation et appreciation d'un batiment. Plusieurs tendances et ambiances pour chaque piece et offre par suite une beaute interne" />
    <meta name="theme-color" content="#ffffff">
	
	<link rel="stylesheet" href="inc/concatCSS.php" type="text/css">
	
	<?php include ('../inc/head.php') ?>
	
</head>

<body data-spy="scroll" data-target=".inner-link" data-offset="60">
    <main>
	
		<!--  Menu -->
		<?php include ('menu.php') ?>
		<!--  EndMenu -->
		
		<?php $slides = findSlide(); ?>
		
        <div class="flexslider flexslider-simple h-full loading parallax">
            <ul class="slides">
				<?php $i=1; foreach($slides AS $slide){ ?>
                <li data-zanim-timeline="{}">
                    <section class="py-0 color-white">
						<?php
							if($i == 1){ ?>
								<div class="background-holder overlay overlay-freya"><img style="position:absolute;margin-top: -100px;" src="assets/images/architecture-design.jpg" alt="architecture-design" /></div>
							<?php }
						?>
						<?php
							if($i == 2){ ?>
								<div class="background-holder overlay overlay-freya"><img style="position:absolute;margin-top: -100px;" src="assets/images/decor-salon.jpg" alt="decor-salon" /></div>
							<?php }
						?>
						<?php
							if($i == 3){ ?>
								<div class="background-holder overlay overlay-freya"><img style="position:absolute;margin-top: -100px;" src="assets/images/presentation-eclairage.jpg" alt="presentation-eclairage" /></div>
							<?php }
						?>
                        <!--/.background-holder-->
                        <div class="container">
                            <div class="row justify-content-start align-items-end pt-11 pb-6 h-full" data-zanim-timeline="{}">
                                <div class="col pb-lg-0">
                                    <div class="row align-items-end parallax" data-rellax-speed="2">
                                        <div class="col-lg">
                                            <div class="overflow-hidden">
                                                <p class="mb-1 text-uppercase ls" data-zanim='{"from":{"opacity":0,"x":-30},"to":{"opacity":1,"x":0},"delay":0.1}'><?php echo $slide->smallDescription; ?></p>
                                            </div>
											<div class="overflow-hidden">
                                                <h2 class="color-white fw-500 mb-4 mb-lg-0" data-zanim='{"from":{"opacity":0,"x":-30},"to":{"opacity":1,"x":0},"delay":0}'><?php echo $slide->description; ?></h2>
                                            </div>
                                        </div>
                                        <div class="col text-lg-right">
                                            <div class="overflow-hidden">
                                                <div data-zanim='{"from":{"opacity":0,"x":-30},"to":{"opacity":1,"x":0},"delay":0.2}'>
                                                    <a class="btn btn-sm btn-outline-white" href="#"><?php echo $slide->link; ?></a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--/.row-->
                        </div>
                        <!--/.container-->
                    </section>
                </li>
				<?php $i++; } ?>
            </ul>
        </div>
		
        <section class="text-center background-white">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <h1 class="mb-4"><?php echo $title->title; ?></h1>
                        <h5><?php echo $title->description; ?></h5>
                    </div>
                </div>
                <div class="row mt-6">
					<?php $inspirationAccueils = findInspirationAccueil(); ?>
					<?php foreach($inspirationAccueils AS $inspirationAccueil){ ?>
                    <div class="col-lg-4 mt-4 mt-lg-0">
                        <div class="row align-items-center">
                            <div class="col-md-6 col-lg-12">
								<a href="<?php echo getUrl($inspirationAccueil->inspiration); ?>-inspiration<?php echo $inspirationAccueil->idInspiration; ?>"><img class="w-100" src="assets/allImages/<?php echo $inspirationAccueil->imageAccueil; ?>" alt="<?php echo $inspirationAccueil->descriptionImageAccueil; ?>" /></a>
                            </div>
                            <div class="col-md-6 col-lg-12 text-md-left text-lg-center">
                                <h4 class="ls text-uppercase mt-4 mb-3"><?php echo $inspirationAccueil->inspiration; ?></h4>
                                <h5 class="text-justify"><?php echo $inspirationAccueil->presentation; ?> <a class="color-info" href="<?php echo getUrl($inspirationAccueil->inspiration); ?>-inspiration<?php echo $inspirationAccueil->idInspiration; ?>"> En savoir plus
									<span class="d-inline-block">&xrarr;</span></a></h5>
                            </div>
                        </div>
                    </div>
					<?php } ?>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
		
		<?php $pieceAccueils = findPieceAccueil(); ?>
		<?php foreach($pieceAccueils AS $pieceAccueil){ ?>
        <section class="py-7 overflow-hidden text-center">
            <div class="background-holder overlay overlay-1 parallax" data-rellax-percentage="0.5"><img style="margin-top: -175px;" src="assets/allImages/<?php echo $pieceAccueil->backImage; ?>" alt="<?php echo $pieceAccueil->descriptionBackImage; ?>" /></div>
            <!--/.background-holder-->
            <div class="container">
                <div class="row" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="col">
                        <div class="overflow-hidden">
                            <h2 class="fs-5 fs-sm-6 color-white mb-3" data-zanim='{"delay":0}'><?php echo $pieceAccueil->piece; ?></h2>
                        </div>
                        <div class="overflow-hidden">
                            <h4 class="ls fw-300 text-uppercase color-9 mb-0" data-zanim='{"delay":0.1}'><?php echo $pieceAccueil->smallDescription; ?></h4>
                        </div>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <section class="text-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10 col-lg-8">
                        <h3 class="mb-4"><?php echo $pieceAccueil->title; ?></h3>
                        <h5 class="mb-7"><?php echo $pieceAccueil->presentation; ?> </br> <a class="color-info" href="<?php echo getUrl($pieceAccueil->piece); ?>-piece<?php echo $pieceAccueil->idPiece; ?>"> En savoir plus <span class="d-inline-block">&xrarr;</span></a></h5>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-6 mb-4">
                        <a href="<?php echo getUrl($pieceAccueil->piece); ?>-piece<?php echo $pieceAccueil->idPiece; ?>"><img class="w-100" src="assets/allImages/<?php echo $pieceAccueil->image1; ?>" alt="<?php echo $pieceAccueil->descriptionImage1; ?>" /></a>
                    </div>
                    <div class="col-lg-6 mb-4">
                        <a href="<?php echo getUrl($pieceAccueil->piece); ?>-piece<?php echo $pieceAccueil->idPiece; ?>"><img class="w-100" src="assets/allImages/<?php echo $pieceAccueil->image2; ?>" alt="<?php echo $pieceAccueil->descriptionImage2; ?>" /></a>
                    </div>
                    <div class="col-12 mb-4">
                        <a href="<?php echo getUrl($pieceAccueil->piece); ?>-piece<?php echo $pieceAccueil->idPiece; ?>"><img class="w-100" src="assets/allImages/<?php echo $pieceAccueil->image3; ?>" alt="<?php echo $pieceAccueil->descriptionImage3; ?>" /></a>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
		<?php } ?>
		
        <!--  Footer -->
		<?php include ('footer.php') ?>
		<!--  EndFooter -->
		
    </main>
    <!--  -->
    <!--    JavaScripts-->
    <!--    =============================================-->
    <?php include ('../inc/javascript.php') ?>
</body>
<!-- Mirrored from markup.themewagon.com/tryfreya/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Mar 2018 09:18:20 GMT -->

</html>