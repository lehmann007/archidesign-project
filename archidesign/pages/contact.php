<?php
	require('../inc/utilitaire.php');
	require('../inc/connectpdo.php');
	require('../inc/fonction.php');
?>
<!DOCTYPE html>
<html lang="en-US">
<!-- Mirrored from markup.themewagon.com/tryfreya/contact-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Mar 2018 09:28:47 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="contact, information, rue" />
    <title>Contact - Information | Archi-Design</title>
    <meta name="theme-color" content="#ffffff">
	<meta name="author" content="Manoa Razafimahenina" />
	
	<link rel="stylesheet" href="inc/concatCSS.php" type="text/css">
	
	<?php include ('../inc/head.php') ?>
</head>

<body data-spy="scroll" data-target=".inner-link" data-offset="60">
    <main>
		
		<!--  Menu -->
		<?php include ('menu.php') ?>
        <section class="py-9 overflow-hidden text-center">
			<div class="background-holder overlay overlay-1 parallax"><img style="margin-top: -160px;" src="assets/images/contact.jpg" alt="contact" /></div>
            <!--/.background-holder-->
            <div class="container">
                <div class="row" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="col">
                        <div class="overflow-hidden">
                            <h1 class="fs-5 fs-sm-6 color-white mb-3" data-zanim='{"delay":0}'>Contact</h1>
                        </div>
                        <div class="overflow-hidden">
                            <h5 class="fs-2 fw-300 ls color-8 text-uppercase" data-zanim='{"delay":0.1}'>Information</h5>
                        </div>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <section class="background-11">
            <div class="container">
                <div class="row align-items-stretch justify-content-center">
                    <div class="col-lg-4 mb-4">
                        <div class="row">
							<?php $contactInfos = findContactInfo(); ?>
							<?php foreach($contactInfos AS $contactInfo){ ?>
                            <div class="col-md-6 col-lg-12">
                                <div class="mb-4 px-5 py-4 background-white">
                                    <h4 class="mb-3"><?php echo $contactInfo->nom; ?></h4>
									<h5><?php echo $contactInfo->rue; ?>
                                    <br><?php echo $contactInfo->lieu; ?>
                                    <br><?php echo $contactInfo->email; ?>
                                    <br>
                                    <a href="tel:<?php echo $contactInfo->phone; ?>"><?php echo $contactInfo->phone; ?></a>
									</h5>
                                </div>
                            </div>
							<?php } ?>
                            <div class="col-12">
                                <div class="px-5 py-4 background-white">
                                    <h4>Socials</h4>
                                    <a class="d-inline-block mt-2" href="#">
                                        <span class="fa fa-linkedin-square fs-2 mr-2 color-primary"></span>
                                    </a>
                                    <a class="d-inline-block mt-2" href="#">
                                        <span class="fa fa-twitter-square fs-2 mx-2 color-primary"></span>
                                    </a>
                                    <a class="d-inline-block mt-2" href="#">
                                        <span class="fa fa-facebook-square fs-2 mx-2 color-primary"></span>
                                    </a>
                                    <a class="d-inline-block mt-2" href="#">
                                        <span class="fa fa-google-plus-square fs-2 ml-2 color-primary"></span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 mb-4">
                        <div class="background-white p-5">
                            <div class="googlemap" data-latlng="-18.899331,47.5208098,17" data-scrollwheel="false" data-icon="assets/images/map-marker.png"
                                data-zoom="16" data-theme="Silver" style="min-height: 427.17px;">
                                <div class="marker-content py-3">
                                    <h5>Eiffel Tower</h5>
                                    <p>Gustave Eiffel's iconic, wrought-iron 1889 tower,
                                        <br> with steps and elevators to observation decks.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <!--  Footer -->
		<?php include ('footer.php') ?>
		<!--  EndFooter -->
    </main>
    <!--  -->
    <!--    JavaScripts-->
    <!--    =============================================-->
    <?php include ('../inc/javascript.php') ?>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCoK8GIrOHzHwnzHCyqrdtmTpUWcdrTTD8&amp;callback=initMap" async></script>
	<script src="assets/js/googlemap.js"></script>
</body>
<!-- Mirrored from markup.themewagon.com/tryfreya/contact-2.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Mar 2018 09:28:47 GMT -->

</html>