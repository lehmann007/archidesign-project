<!DOCTYPE html>
<html lang="en-US">
<!-- Mirrored from markup.themewagon.com/tryfreya/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Mar 2018 09:23:12 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	
    <title>Page introuvable | Archi-Design</title>
    <meta name="theme-color" content="#ffffff">
	
    <link rel="stylesheet" href="inc/concatCSS.php" type="text/css">
	
	<?php include ('../inc/head.php') ?>
</head>

<body data-spy="scroll" data-target=".inner-link" data-offset="60">
    <main>
        <section class="text-center py-0">
            <div class="background-holder overlay overlay-Freya" style="background-image:url(assets/images/404.jpg);"></div>
            <!--/.background-holder-->
            <div class="container">
                <div class="row h-full align-items-center color-white">
                    <div class="col">
                        <h1 class="fs-4 fs-sm-6 fs-md-8 color-white">404</h1>
                        <h5 class="text-uppercase ls color-white fs-0 fs-md-1">Oops! La page que vous cherchee est introuvable !</h5>
                        <a class="btn btn-primary btn-capsule mt-4 btn-icon-pop" href="accueil">
                            <span class="fa fa-home mr-2"></span>Retourner a la page d'accueil</a>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
    </main>
    <!--  -->
    <!--    JavaScripts-->
    <!--    =============================================-->
    <?php include ('../inc/javascript.php') ?>
    <!-- Hotjar Tracking Code for http://markup.themewagon.com/tryfreya/-->
    <script>(function (h, o, t, j, a, r) {
            h.hj = h.hj || function () { (h.hj.q = h.hj.q || []).push(arguments) };
            h._hjSettings = { hjid: 731669, hjsv: 6 };
            a = o.getElementsByTagName('head')[0];
            r = o.createElement('script'); r.async = 1;
            r.src = t + h._hjSettings.hjid + j + h._hjSettings.hjsv;
            a.appendChild(r);
        })(window, document, 'https://static.hotjar.com/c/hotjar-', '.js?sv=');

    </script>
    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-76729372-6"></script>
    <script>window.dataLayer = window.dataLayer || [];
        function gtag() { dataLayer.push(arguments); }
        gtag('js', new Date());
        gtag('config', 'UA-76729372-6');
    </script>
    <script src="assets/js/core.js"></script>
    <script src="assets/js/main.js"></script>
</body>
<!-- Mirrored from markup.themewagon.com/tryfreya/404.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Mar 2018 09:24:11 GMT -->

</html>