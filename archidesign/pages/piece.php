<?php
	require('../inc/utilitaire.php');
	require('../inc/connectpdo.php');
	require('../inc/fonction.php');
	$piecePage = findPiecePage($_GET['id']);
	$pieceTitre = findPieceById($_GET['id']);
	if($_GET['url'] != strtolower($pieceTitre->piece)){
		header('Location: '.getUrl($pieceTitre->piece).'-piece'.$pieceTitre->id);
	}
?>
<!DOCTYPE html>
<html lang="en-US">
<!-- Mirrored from markup.themewagon.com/tryfreya/projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Mar 2018 09:24:44 GMT -->

<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="keywords" content="piece" />
    <title>Piece : <?php echo $pieceTitre->piece ?> | Archi-Design</title>
	<meta name="description" content="L’amenagement de votre piece obeit a un protocole bien etabli. Chaque presentation evoque un element cle dans la finalisation" />
    <meta name="theme-color" content="#ffffff">
	
	<link rel="stylesheet" href="inc/concatCSS.php" type="text/css">
	
	<?php include ('../inc/head.php') ?>
</head>

<body data-spy="scroll" data-target=".inner-link" data-offset="60">
    <main>
	
        <!--  Menu -->
		<?php include ('menu.php') ?>
		<!--  EndMenu -->
		
        <section class="py-9 overflow-hidden text-center">
			<div class="background-holder overlay overlay-1 parallax"><img style="margin-top: -160px;" src="assets/images/piece-presentation.jpg" alt="piece-presentation" /></div>
            <!--/.background-holder-->
            <div class="container">
                <div class="row" data-zanim-timeline="{}" data-zanim-trigger="scroll">
                    <div class="col">
                        <div class="overflow-hidden">
                            <h1 class="fs-5 fs-sm-6 color-white mb-3" data-zanim='{"delay":0}'>Piece : <?php echo $pieceTitre->piece ?></h1>
                        </div>
                        <div class="overflow-hidden">
                            <h5 class="fs-2 fw-300 ls color-8 text-uppercase" data-zanim='{"delay":0.1}'><?php echo $pieceTitre->piece ?></h5>
                        </div>
                    </div>
                </div>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
        <section>
            <div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-11">
						<div class="border-bottom border-color-9 pb-6 mb-6">
							<div class="row justify-content-center">
								<h2 class="mb-4"><?php echo $piecePage->title; ?></h3>
							</div>
							<div class="row justify-content-center">
								<h5 class="mb-7"><?php echo $piecePage->presentation; ?></h5>
							</div>
						</div>
					</div>
				</div>
					
				<?php $pieceInformations = findPieceInformation($_GET['id']); ?>
				<?php foreach($pieceInformations AS $pieceInformation){ ?>
                <div class="row justify-content-center">
                    <div class="col-lg-11">
                        <div class="border-bottom border-color-9 pb-6 mb-6">
                            <div class="row justify-content-center">
                                <div class="col-lg-4">
                                    <h3>
										<span class="d-inline-block"><?php echo $pieceInformation->information; ?></span>
                                    </h3>
                                </div>
                                <div class="col text-justify">
										<img src="assets/allImages/<?php echo $pieceInformation->image; ?>" alt="<?php echo $pieceInformation->descriptionImage; ?>">
                                    <h5 class="mt-4"><?php echo $pieceInformation->paragraphe; ?>
                                    </h5>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
				<?php } ?>
                <!--/.row-->
            </div>
            <!--/.container-->
        </section>
		
        <!--  Footer -->
		<?php include ('footer.php') ?>
		<!--  EndFooter -->
		
    </main>
    <!--  -->
    <!--    JavaScripts-->
    <!--    =============================================-->
    <?php include ('../inc/javascript.php') ?>
</body>
<!-- Mirrored from markup.themewagon.com/tryfreya/projects.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 29 Mar 2018 09:25:50 GMT -->

</html>