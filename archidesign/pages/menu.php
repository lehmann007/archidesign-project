<?php 
	$nameProject = findNameProject();
	$menus = findMenu();
?>

<div class="loading" id="preloader">
    <div class="h-100 d-flex align-items-center justify-content-center">
        <div class="loader-box">
            <div class="loader"></div>
        </div>
    </div>
</div>
<div class="znav-container znav-white znav-freya znav-fixed" id="znav-container">
    <div class="container">
        <nav class="navbar navbar-expand-lg">
            <a class="navbar-brand overflow-hidden pr-3" href="accueil">
                <div class="background-1 radius-br-0 radius-secondary lh-1 color-white fs-0" style="padding: 7px 10px 7px 13px;"><?php echo $nameProject->name ?></div>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
                aria-expanded="false" aria-label="Toggle navigation">
                <div class="hamburger hamburger--emphatic">
                    <div class="hamburger-box">
                        <div class="hamburger-inner"></div>
                    </div>
                </div>
            </button>
            <div class="collapse navbar-collapse" id="navbarNavDropdown">
                <ul class="navbar-nav fs-0">
                    <li>
                        <a href="accueil"><?php echo $menus[0]->menu ?></a>
                    </li>
                    <li>
                        <a href="JavaScript:void(0)"><?php echo $menus[1]->menu ?></a>
						<?php $inspirations = findInspiration(); ?>
                        <ul class="dropdown">
							<?php foreach($inspirations AS $inspiration){ ?>
								<li>
									<a href="<?php echo getUrl($inspiration->inspiration); ?>-inspiration<?php echo $inspiration->id; ?>"><?php echo $inspiration->inspiration; ?></a>
								</li>
							<?php } ?>
                        </ul>
                    </li>
                    <li>
                        <a href="JavaScript:void(0)"><?php echo $menus[2]->menu ?> </a>
						<?php $pieces = findPiece(); ?>
                        <ul class="dropdown">
							<?php foreach($pieces AS $piece){ ?>
								<li>
									<a href="<?php echo getUrl($piece->piece); ?>-piece<?php echo $piece->id; ?>"><?php echo $piece->piece; ?></a>
								</li>
							<?php } ?>
                        </ul>
                    </li>
                    <li>
                        <a href="<?php echo getUrl($menus[3]->menu); ?>"><?php echo $menus[3]->menu ?></a>
                    </li>
                    <li>
                        <a href="contact"><?php echo $menus[4]->menu ?></a>
                    </li>
                </ul>
				<ul id="navlist">
					<li id="accueil"><a href="accueil"></a></li>
					<li id="couleur"><a href="couleurs-et-eclairages"></a></li>
					<li id="contact"><a href="contact"></a></li>
				</ul>
            </div>
        </nav>
    </div>
</div>