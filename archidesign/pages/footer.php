<section class="py-4 fs-1 text-center background-9">
	<div class="container">
		<div class="row">
			<div class="col">
				<ul id="navlist2">
					<li id="facebook"><a href="accueil"></a></li>
					<li id="instagram"><a href="couleurs-et-eclairages"></a></li>
					<li id="twitter"><a href="contact"></a></li>
				</ul>
			</div>
		</div>
		<!--/.row-->
	</div>
	<!--/.container-->
</section>

<section class="background-primary text-center py-6">
	<div class="container">
		<div class="row justify-content-center">
			<div class="col">
				<?php $contactFooter = findContactInfo(); ?>
				<p class="text-uppercase color-9 ls mb-3"><?php echo $contactFooter[0]->nom; ?>, <?php echo $contactFooter[0]->rue; ?>, <?php echo $contactFooter[0]->lieu; ?>, <?php echo $contactFooter[0]->phone; ?></p>
				<p class="color-5 mb-0">Projet Web Design par : Razafimahenina Manoa n.43 ETU000537 </p>
			</div>
		</div>
		<!--/.row-->
	</div>
	<!--/.container-->
</section>